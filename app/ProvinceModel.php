<?php

namespace App;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProvinceModel extends Model
{
    protected $primaryKey = 'prov_id';
    protected $table = 'reff_province';

    use SoftDeletes;
    protected $dates = ['deleted_at'];

     public function deleteData(Request $request, $id){
        $bean = $this->find($id);
        $bean->delete($id);
    }
}
