<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Autonumber extends Model
{
    
    public static function autonumber($barang,$primary,$prefix,$prefixid){
        $q=DB::table($barang)->select(DB::raw('MAX(RIGHT('.$primary.',4)) as kd_max'))->where('company_id',$prefixid);
        //$prx=$prefix.Date('dmY');
        $prx=$prefix;
        
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
             
                $tmp = ((int)$k->kd_max)+1;
                $kd = $prx.sprintf("%04s", $tmp);
            }
        }
        else
        {
            $kd = $prx."0001";
        }

        return $kd;
    }

    public static function autonumber_($barang,$primary){
        $q=DB::table($barang)->select(DB::raw('MAX('.$primary.') as kd_max'));
        //$prx=$prefix.Date('dmY');
        //$prx=$prefix;
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
             
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%03s", $tmp);
                //$kd = $tmp;
            }
        }
        else
        {
            $kd ="001";
        }

        return $kd;
    }

    


    
}
