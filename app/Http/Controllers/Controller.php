<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use GuzzleHttp\Client;;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Collection;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendResponse($code, $msg, $data = null){
        switch ($code){
            case 0:
                return json_encode(['code' => $code, 'msg' => $msg, 'data' => $data->errors()]);
                break;
            case 1:
                return json_encode(['code' => $code, 'msg' => $msg, 'data' => $data]);
                break;       
            case 2:
                return json_encode(['code' => $code, 'msg' => $msg]);
                break;           

        }
        return ;
    }

      public function responseData($code, $message){
        return ['code' => $code, 'message'=> $message];
    }

    public function weekday_diff($from, $to, $normalise=true) {
        $_from = is_int($from) ? $from : strtotime($from);
        $_to   = is_int($to) ? $to : strtotime($to);

        if($_from == $_to) return 0;
        
        // normalising means partial days are counted as a complete day.
        if ($normalise) {
            $_from = strtotime(date('Y-m-d', $_from));
            $_to = strtotime(date('Y-m-d', $_to));
        }
        $all_days = @range($_from, $_to, 60*60*24);
        if (empty($all_days)) return 0;
        $week_days = array_filter(
            $all_days,
            create_function('$t', '$d = date("w", strtotime("+{$t} seconds", 0)); return !in_array($d, array(0,6));')
        );
        return count($week_days);
    }
}
