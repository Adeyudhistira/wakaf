<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\ProjectModel;
use App\NazhirModel;
use App\GalleryModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
class ProjectController extends Controller
{


   public function index(Request $request)
    {
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY project_id) AS nomor_urut,rc.category_desc,rp.prov_name,rct.city_name
        ,rs.definition,p.*,n.comp_name
        FROM project p
        join reff_category rc on rc.category_id=p.category_id
        join reff_province rp on rp.prov_id=p.prov_id
        join reff_city rct on rct.city_id=p.city_id
        join reff_status rs on rs.id=p.status_project
        join nazhir n on n.users_id=p.users_id
        where p.deleted_at is null
        ");
        $param['data']=$query;
        if ($request->ajax()) {
            $view = view('project.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'project.index',$param);
    }

      public function detail($id,Request $request)
    {
        $query = \DB::select("SELECT rc.category_desc,rp.prov_name,rct.city_name,
          rs.definition,p.*,n.comp_name,g.img1,g.img2,g.img3
        FROM project p
        join reff_category rc on rc.category_id=p.category_id
        join reff_province rp on rp.prov_id=p.prov_id
        join reff_city rct on rct.city_id=p.city_id
        join reff_status rs on rs.id=p.status_project
        join nazhir n on n.users_id=p.users_id
        join gallery g on g.project_id=p.project_id
        where p.project_id=:id
        ",['id'=>$id]);

        $param['data']=$query;

        if ($request->ajax()) {
            $view = view('project.detail',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'project.detail',$param);
    }



    /*
    <a href='#' title='Edit' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i></a>
            <a style='color:red;' title='Hapus' href='#' onclick='hapus($query->id,\"user/delete\");'><i class='fa fa-trash'></i></a>
            <a style='color:green;' title='Reset' href='#' onclick=resetPassword(".$query->id.");><i class='fa fa-refresh'></i></a>";
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



    if($request->hasFile('logo')){
         $destination_path = public_path('uploadimg');
         $files = $request->file('logo');
         $filename = $files->getClientOriginalName();
         $upload_success = $files->move($destination_path, $filename);

        $Client = new ProjectModel();
        $Client->category_id = $request->input('category_id');
        $Client->prov_id = $request->input('prov_id');
        $Client->city_id = $request->input('city_id');
        $Client->users_id = $request->input('users_id');
        $Client->nama_project = $request->input('nama_project');
        $Client->deskripsi_project = $request->input('deskripsi_project');
        $Client->target_dana = str_replace(",","", $request->input('target_dana'));
        $Client->status_project = $request->input('status_project');
        $Client->jenis_wakaf = $request->input('jenis_wakaf');
        $Client->crt_dt = $request->input('crt_dt');
        $Client->end_date = $request->input('end_date');
        $Client->main_img = $filename;
        $Client->progress = 0;
        $Client->total_funding = 0;
        $Client->total_wakif = 0;
        $Client->save();

     }else{
        $Client = new ProjectModel();
        $Client->category_id = $request->input('category_id');
        $Client->prov_id = $request->input('prov_id');
        $Client->city_id = $request->input('city_id');
        $Client->users_id = $request->input('users_id');
        $Client->nama_project = $request->input('nama_project');
        $Client->deskripsi_project = $request->input('deskripsi_project');
        $Client->target_dana = str_replace(",","", $request->input('target_dana'));
        $Client->status_project = $request->input('status_project');
        $Client->jenis_wakaf = $request->input('jenis_wakaf');
        $Client->crt_dt = $request->input('crt_dt');
        $Client->end_date = $request->input('end_date');
        $Client->progress = 0;
        $Client->total_funding = 0;
        $Client->total_wakif = 0;
        $Client->save();
     }

         $destination_path = public_path('uploadimg');
         $files1 = $request->file('img1');
         $filename1 = $files1->getClientOriginalName();
         $upload_success = $files1->move($destination_path, $filename);

         $destination_path = public_path('uploadimg');
         $files2 = $request->file('img2');
         $filename2 = $files2->getClientOriginalName();
         $upload_success = $files2->move($destination_path, $filename2);


         $destination_path = public_path('uploadimg');
         $files3 = $request->file('img3');
         $filename3 = $files3->getClientOriginalName();
         $upload_success = $files3->move($destination_path, $filename3);

        $project = \DB::select("select * from project where nama_project like'".$request->input('nama_project')."%'");

        $Client = new GalleryModel();
        $Client->project_id = $project[0]->project_id;
        $Client->img1 = $filename1;
        $Client->img2 = $filename2;
        $Client->img3 = $filename3;
        $Client->save();

     $nazhir = \DB::select("select * from nazhir where users_id =:id",['id'=>$request->input('users_id')]);

     $url = env('API_BASE_URL')."institution";
       $client = new Client();
       $data = array('institution_name'=> $nazhir[0]->comp_name,'address' => $nazhir[0]->alamat,'pic_name'=>$nazhir[0]->admin_name,'current_account'=>$request->input('giro'),'escrow_account'=>$request->input('giro1'),'branch_id'=>'001','product_name'=>$request->input('nama_project'));
                   //var_dump($data);
     $headers = [
            'Content-Type' => 'application/json'
            //'Authorization' => Auth::id()
        ];
        var_dump($data);
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,


            ]);
            //return redirect('getConfirm/' . $va_acc_no . '/'. $bill_amount .'/'. $timeout .'/'. $expire_at );
            //return json_decode($result->getBody());
            //return $this->responseData(1, json_decode($result->getBody()));

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
            //var_dump($data['massege']);exit;
         // var_dump($data['product_id']);
          //var_dump($data['intitusi_id']);


            return $this->sendResponse('1','Input Project berhasil',$Client);
            //return $this->sendResponse('1','Payment Berhasil',json_decode($result->getBody()));
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            return $this->responseData($e->getCode(), $response);
        }



    //   return $this->sendResponse('1','Input Project berhasil',$Client);
    }


    public function delete(Request $request, $id){


        try{
            $model = new ProjectModel();
            $model->deleteData($request, $id);
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
            }else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        }catch (CustomException $e){
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }

    }

    public function findRealById($id){
        $data = \DB::select("SELECT project.*,g.img1,g.img2,g.img3 FROM project join gallery g using (project_id)  WHERE project_id =".$id);
        return json_encode($data);
    }

    public function update(Request $request){

      // var_dump($request->all());


      if($request->hasFile('logo')){
        $destination_path = public_path('uploadimg');
       $files = $request->file('logo');
       $filename = $files->getClientOriginalName();
       $upload_success = $files->move($destination_path, $filename);
     } else {
       $filename = $request->input('logo_lama');;
     }

     if($request->hasFile('img_satu')){
       $destination_path = public_path('uploadimg');
      $files2 = $request->file('img_satu');
      $filename2 = $files2->getClientOriginalName();
      $upload_success = $files2->move($destination_path, $filename2);
    } else {
      $filename2 = $request->input('gp_lama1');;
    }

    if($request->hasFile('img_dua')){
      $destination_path = public_path('uploadimg');
     $files3 = $request->file('img_dua');
     $filename3 = $files3->getClientOriginalName();
     $upload_success = $files3->move($destination_path, $filename3);
   } else {
     $filename3 = $request->input('gp_lama2');;
   }

   if($request->hasFile('img_tiga')){
     $destination_path = public_path('uploadimg');
    $files4 = $request->file('img_tiga');
    $filename4 = $files4->getClientOriginalName();
    $upload_success = $files4->move($destination_path, $filename4);
  } else {
    $filename4 = $request->input('gp_lama3');;
  }


    //  if($request->hasFile('img1')){
    //   $files = $request->file('img1');
    //   $filename_img1 = $files->getClientOriginalName();
    //   $upload_success = $files->move($destination_path, $filename_img1);
    // } else {
    //   $filename_img1 = $request->input('gp_lama1');;
    // }



        $Client = ProjectModel::find($request->input('id'));
        $Client->category_id = $request->input('category_id');
        $Client->prov_id = $request->input('prov_id');
        $Client->city_id = $request->input('city_id');
        $Client->users_id = $request->input('users_id');
        $Client->nama_project = $request->input('nama_project');
        $Client->deskripsi_project = $request->input('deskripsi_project');
        $Client->jenis_wakaf = $request->input('jenis_wakaf');
        $Client->target_dana = str_replace(",","", $request->input('target_dana'));
        $Client->status_project = $request->input('status_project');
        $Client->crt_dt = $request->input('crt_dt');
        $Client->end_date = $request->input('end_date');
        $Client->main_img = $filename;
        $Client->save();
        //GalleryModel
        GalleryModel::where('project_id',$request->input('id'))
          ->update(
            [
              'img1' => $filename2,
              'img2' => $filename3,
              'img3' => $filename4
            ]);


        return $this->sendResponse(1,'Berhasil Diupdate', $Client);
    }


}
