<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\NazhirModel;
use App\UserModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
class NazhirController extends Controller
{


   public function index(Request $request)
    {
$query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY n.users_id) AS nomor_urut,n.*,u.prov_id,u.city_id,rc.city_name,rp.prov_name
            FROM nazhir n left join tb_users u on u.users_id=n.users_id
                        left join reff_city rc on rc.city_id=u.city_id
                        left join reff_province rp on rp.prov_id=u.prov_id where n.deleted_at is null");

        $param['data']=$query;
        if ($request->ajax()) {
            $view = view('nazhir.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nazhir.index',$param);
    }


    public function table(Request $request){
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY n.users_id) AS nomor_urut,n.*,u.prov_id,u.city_id,rc.city_name,rp.prov_name
            FROM nazhir n left join tb_users u on u.users_id=n.users_id
                        left join reff_city rc on rc.city_id=u.city_id
                        left join reff_province rp on rp.prov_id=u.prov_id");

        $data = Datatables::of($query)->addColumn('action', function ($query){
            return '<button type="button" class="btn btn-info bg-blue" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" onclick="editshow('.$query->users_id.')">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-danger bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus" onclick=hapus('.$query->users_id.',"nazhir/delete")>
                        <i class="fa fa-trash"></i>
                    </button>';

        })->make(true);

        return $data;
    }

    /*
    <a href='#' title='Edit' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i></a>
            <a style='color:red;' title='Hapus' href='#' onclick='hapus($query->id,\"user/delete\");'><i class='fa fa-trash'></i></a>
            <a style='color:green;' title='Reset' href='#' onclick=resetPassword(".$query->id.");><i class='fa fa-refresh'></i></a>";
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Client = new UserModel();
        $Client->prov_id = $request->input('prov_id');
        $Client->city_id =  $request->input('city_id');
        $Client->email =  $request->input('email');
        $Client->role_id = 3;
        $Client->phone_no = $request->input('phone_no');
        $Client->password = 1;
        $Client->save();

        $idprod = \DB::select("select * from tb_users where email like'".$request->input('email')."%'");


        if($request->hasFile('logo')){
         $destination_path = public_path('uploadimg');
         $files = $request->file('logo');
         $filename = $files->getClientOriginalName();
         $upload_success = $files->move($destination_path, $filename);

        $Client = new NazhirModel();
        $Client->users_id = $idprod[0]->users_id;
        $Client->comp_name = $request->input('comp_name');
        $Client->alamat =  $request->input('alamat');
        $Client->comp_desc =  $request->input('comp_desc');
        $Client->about =  $request->input('about');

        $Client->admin_name =  $request->input('admin_name');
        $Client->total_project =  0;
        $Client->logo =  $filename;
        $Client->save();
        }else{
        $Client = new NazhirModel();
        $Client->users_id = $idprod[0]->users_id;
        $Client->comp_name = $request->input('comp_name');
        $Client->alamat =  $request->input('alamat');
        $Client->comp_desc =  $request->input('comp_desc');
        $Client->about =  $request->input('about');
        $Client->admin_name =  $request->input('admin_name');
        $Client->total_project =  0;
        $Client->save();
        }

        /*
        $idprod = \DB::select("select * from nazhir where comp_name like'".$request->input('comp_name')."%'");
        $Client = new UserModel();
        $Client->users_id = $idprod[0]->users_id;
        $Client->prov_id = $request->input('prov_id');
        $Client->city_id =  $request->input('city_id');
        $Client->email =  $request->input('email');
        $Client->role_id = 3;
        $Client->phone_no = $request->input('phone_no');
        $Client->password = 1;
        $Client->save();
        */

        return $this->sendResponse('1','Input Nazhir berhasil',$Client);
    }


    public function delete(Request $request, $id){

        try{
            $model = new NazhirModel();
            $model->deleteData($request, $id);
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
            }else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        }catch (CustomException $e){
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }

    }

    public function findRealById($id){
        $data = \DB::select("SELECT * FROM nazhir
join tb_users on tb_users.users_id=nazhir.users_id
WHERE nazhir.users_id=".$id);
        return json_encode($data);
    }

    public function update(Request $request){

        // $validators = \Validator::make($request->all(),['definition'=>'required','seq'=>'required']);
        // if($validators->fails()){
        //     return $this->sendResponse(0,'Update Gagal', $validators);
        // }

      //  var_dump($request->all());

      if($request->hasFile('logo')){
       $destination_path = public_path('uploadimg');
       $files = $request->file('logo');
       $filename = $files->getClientOriginalName();
       $upload_success = $files->move($destination_path, $filename);
     } else {
       $filename = $request->input('logo_lama');;
     }

        $Client = NazhirModel::find($request->input('id'));

        $Client->comp_name = $request->input('comp_name');
        $Client->alamat = $request->input('alamat');
        $Client->comp_desc = $request->input('comp_desc');
        $Client->about = $request->input('about');
        $Client->admin_name = $request->input('admin_name');
        $Client->logo =  $filename;

        $Client->save();

        $Client = UserModel::find($request->input('id'));
        $Client->prov_id = $request->input('prov_id');
        $Client->city_id = $request->input('city_id');
        $Client->email = $request->input('email');
        $Client->phone_no = $request->input('phone_no');
        $Client->save();


        return $this->sendResponse(1,'Berhasil Diupdate', $Client);
    }


}
