<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LaporanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   
     public function transaksi(Request $request)
    {
        $flt=false;
        $query="";
        
        if($request->has('filtercari')){
            $flt=true;
           // if($request->has('tglmulai')) $qry->whereBetween('created_at', [$request->input('tglmulai'), $request->input('tglselesai')]);
             $query = \DB::select("
                        select n.comp_name,p.nama_project,t.no_va,t.status,t.nominal from transaksi t
                        join project p on p.project_id=t.project_id
                        join nazhir n on n.users_id=p.users_id where p.users_id=:id and t.crt_dt between '". $request->input('tglmulai') ."' and '". $request->input('tglselesai') ." 24:00:00' order by t.crt_dt asc" ,['id'=>$request->input('users_id')]);
            } 
        //$qry->orderBy('created_at');


        //var_dump($qry);
        $param['flt']=$flt;
        $param['data']=$query;
        if ($request->ajax()) {
            $view = view('laporan.lap_transaksi',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'laporan.lap_transaksi',$param);
    }

}
