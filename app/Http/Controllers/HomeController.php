<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
      public function index(Request $request)
    {
        $data = \DB::select("select t.no_va,p.nama_project,t.status,t.crt_dt,t.nominal from project p
                    join transaksi t on p.project_id=t.project_id");

        $total = \DB::select("select sum(paid) as paid , sum(wait) as wait from(
                    select 0 as paid,count(id_trx) as wait from transaksi where status='wait_payment'
                    union all
                    select count(id_trx) as paid, 0 as wait from transaksi where status='paid'
                    ) as y");

        $totalnazhir = \DB::select("select count(users_id) as value from nazhir");

        $totalproject = \DB::select("select count(users_id) as value from project");

        $totaldana = \DB::select("select sum(nominal) as value from transaksi");

        $ket = \DB::select("select category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            GROUP BY category_desc,colour");

        
        

        $param['data']=$data;
        $param['ket']=$ket;
        $param['total']=$total;
        $param['totalnazhir']=$totalnazhir;
        $param['totalproject']=$totalproject;
        $param['totaldana']=$totaldana; 
        if ($request->ajax()) {
            $view = view('home',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'home',$param);
    }

    public function kab($id){
        $data = \DB::select("SELECT * FROM reff_city where prov_id=:id",['id'=>$id]);
        return json_encode($data);
    }

    public function utama(){
        $data = \DB::select("select sum(bulan_1) as bulan_1 ,sum(bulan_2) as bulan_2,sum(bulan_3) as bulan_3,sum(bulan_4) as bulan_4,sum(bulan_5) as bulan_5,sum(bulan_6) as bulan_6,sum(bulan_7) as bulan_7,sum(bulan_8) as bulan_8,sum(bulan_9) as bulan_9,sum(bulan_10) as bulan_10,sum(bulan_11) as bulan_11,sum(bulan_12) as bulan_12,category_desc,colour  from(

select sum(nominal) as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 1
                        GROUP BY category_desc,colour

union all


select 0 as bulan_1,sum(nominal) as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 2
                        GROUP BY category_desc,colour


union all


select 0 as bulan_1,0 as bulan_2,sum(nominal) as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 3
                        GROUP BY category_desc,colour

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,sum(nominal) as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 4
                        GROUP BY category_desc,colour

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,sum(nominal) as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 5
                        GROUP BY category_desc,colour

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,sum(nominal) as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12 ,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 6
                        GROUP BY category_desc,colour

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,sum(nominal) as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 7
                        GROUP BY category_desc,colour

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,sum(nominal) as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 8
                        GROUP BY category_desc,colour

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,sum(nominal) as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 9
                        GROUP BY category_desc,colour

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,sum(nominal) as bulan_10,0 as bulan_11,0 as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 10
                        GROUP BY category_desc,colour

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,sum(nominal) as bulan_11,0 as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where EXTRACT(MONTH FROM t.crt_dt)= 11
                        GROUP BY category_desc,colour

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,sum(nominal) as bulan_12,category_desc,colour from transaksi t
            join project p on p.project_id=t.project_id
            join nazhir n on n.users_id=p.users_id
            join reff_category c on c.category_id=p.category_id
            where  EXTRACT(MONTH FROM t.crt_dt)= 12
                        GROUP BY category_desc,colour

) as y
GROUP BY category_desc,colour");
        return json_encode($data);
    }

    public function bar1(){
        $data = \DB::select("select sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-11 months ago') THEN 1 else 0 end) as bulan_1,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-10 months ago') THEN 1 else 0 end) as bulan_2,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-9 months ago') THEN 1 else 0 end) as bulan_3,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-8 months ago') THEN 1 else 0 end) as bulan_4,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-7 months ago') THEN 1 else 0 end) as bulan_5,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-6 months ago') THEN 1 else 0 end) as bulan_6,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-5 months ago') THEN 1 else 0 end) as bulan_7,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-4 months ago') THEN 1 else 0 end) as bulan_8,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-3 months ago') THEN 1 else 0 end) as bulan_9,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-2 months ago') THEN 1 else 0 end) as bulan_10,
            sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-1 months ago') THEN 1 else 0 end) as bulan_11,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now()) THEN 1 else 0 end) as bulan_12 
        from nazhir ");
        return json_encode($data);
    }

    public function bar2(){
        $data = \DB::select("select sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-11 months ago') THEN 1 else 0 end) as bulan_1,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-10 months ago') THEN 1 else 0 end) as bulan_2,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-9 months ago') THEN 1 else 0 end) as bulan_3,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-8 months ago') THEN 1 else 0 end) as bulan_4,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-7 months ago') THEN 1 else 0 end) as bulan_5,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-6 months ago') THEN 1 else 0 end) as bulan_6,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-5 months ago') THEN 1 else 0 end) as bulan_7,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-4 months ago') THEN 1 else 0 end) as bulan_8,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-3 months ago') THEN 1 else 0 end) as bulan_9,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-2 months ago') THEN 1 else 0 end) as bulan_10,
            sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now() - INTERVAL '-1 months ago') THEN 1 else 0 end) as bulan_11,
        sum(case when EXTRACT(MONTH FROM created_at)= EXTRACT(MONTH FROM  now()) THEN 1 else 0 end) as bulan_12 
        from project ");
        return json_encode($data);
    }

    public function bar3(){
        $data = \DB::select("select sum(bulan_1) as bulan_1 ,sum(bulan_2) as bulan_2,sum(bulan_3) as bulan_3,sum(bulan_4) as bulan_4,sum(bulan_5) as bulan_5,sum(bulan_6) as bulan_6,sum(bulan_7) as bulan_7,sum(bulan_8) as bulan_8,sum(bulan_9) as bulan_9,sum(bulan_10) as bulan_10,sum(bulan_11) as bulan_11,sum(bulan_12) as bulan_12  from(

select sum(nominal) as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-11 months ago')

union all

select 0 as bulan_1,sum(nominal) as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-10 months ago')

union all

select 0 as bulan_1,0 as bulan_2,sum(nominal) as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-9 months ago')

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,sum(nominal) as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-8 months ago')

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,sum(nominal) as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-7 months ago')

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,sum(nominal) as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-6 months ago')

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,sum(nominal) as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-5 months ago')

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,sum(nominal) as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-4 months ago')

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,sum(nominal) as bulan_9,0 as bulan_10,0 as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-3 months ago')

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,sum(nominal) as bulan_10,0 as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-2 months ago')

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,sum(nominal) as bulan_11,0 as bulan_12 from transaksi where EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now() - INTERVAL '-1 months ago')

union all

select 0 as bulan_1,0 as bulan_2,0 as bulan_3,0 as bulan_4,0 as bulan_5,0 as bulan_6,0 as bulan_7,0 as bulan_8,0 as bulan_9,0 as bulan_10,0 as bulan_11,sum(nominal) as bulan_12 from transaksi where  EXTRACT(MONTH FROM crt_dt)= EXTRACT(MONTH FROM  now())

) as y
 ");
        return json_encode($data);
    }
}
