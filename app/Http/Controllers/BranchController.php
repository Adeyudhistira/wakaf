<?php
/**
 * Created by PhpStorm.
 * User: ade yudhistira
 * Date: 17/05/2018
 * Time: 11.19
 */

namespace App\Http\Controllers;

use App\BranchModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use App\Autonumber;
use Response;
use DB;
use Hash;
use Auth;
class BranchController extends Controller
{


   public function index(Request $request)
    {
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY id) AS nomor_urut,*
            FROM reff_branch where deleted_at is null");
        $param['data']=$query;
        if ($request->ajax()) {
            $view = view('branch.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'branch.index',$param);
    }


    public function table(Request $request){
        $query = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY n.users_id) AS nomor_urut,n.*,u.prov_id,u.city_id
            FROM nazhir n join tb_users u on u.users_id=n.users_id");

        $data = Datatables::of($query)->addColumn('action', function ($query){
            return '<button type="button" class="btn btn-info bg-blue" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" onclick="editshow('.$query->users_id.')">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-danger bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus" onclick=hapus('.$query->users_id.',"nazhir/delete")>
                        <i class="fa fa-trash"></i>
                    </button>';

        })->make(true);

        return $data;
    }

    /*
    <a href='#' title='Edit' onclick='editshow(".$query->id.")'><i class='fa fa-pencil'></i></a>
            <a style='color:red;' title='Hapus' href='#' onclick='hapus($query->id,\"user/delete\");'><i class='fa fa-trash'></i></a>
            <a style='color:green;' title='Reset' href='#' onclick=resetPassword(".$query->id.");><i class='fa fa-refresh'></i></a>";
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $Client = new BranchModel();
        $Client->branch_name = $request->input('branch_name');
        $Client->cash_acc_no = $request->input('cash_acc_no');
        $Client->save();
        */

        $table="reff_branch";
        $primary="id";
        //$prefix="TR-";
        $kode=Autonumber::autonumber_($table,$primary);

        $product=DB::table('reff_branch')->insert(
            ['id'=>$kode,'branch_name' => $request->input('branch_name'), 'cash_acc_no' => $request->input('cash_acc_no')]
        );
       return $this->sendResponse('1','Input Branch berhasil',$product);
    }


    public function delete(Request $request, $id){


        try{
            $model = new BranchModel();
            $model->deleteData($request, $id);
            return response()->json([
                'rc' => 0,
                'rm' => "Sukses",
            ]);
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data ini sedang dipakai, tidak bisa melakukan hapus data';
                return response()->json([
                    'rc' => 1,
                    'rm' => $rm
                ]);
            }else {
                return response()->json([
                    'rc' => 1,
                    'rm' => $e->errorInfo
                ]);
            }
        }catch (CustomException $e){
            return response()->json([
                'rc' => 1,
                'rm' => $e->getMessage()
            ]);
        }

    }

    public function findRealById($id){
        $data = \DB::select("SELECT * FROM reff_branch WHERE id ="."'".$id."'");
        return json_encode($data);
    }

    public function update(Request $request){

      //  var_dump($request->all());

        $Client = BranchModel::find($request->input('id'));
        $Client->branch_name = $request->input('branch_name');
        $Client->cash_acc_no = $request->input('cash_acc_no');
        $Client->save();


        return $this->sendResponse(1,'Berhasil Diupdate', $Client);
    }


}
