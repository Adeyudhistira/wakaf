<?php

namespace App;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class CityModel extends Model
{
    protected $primaryKey = 'city_id';
    protected $table = 'reff_city';

    use SoftDeletes;
    protected $dates = ['deleted_at'];

     public function deleteData(Request $request, $id){
        $bean = $this->find($id);
        $bean->delete($id);
    }
}
