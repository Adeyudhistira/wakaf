<?php

namespace App;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class UserModel extends Model
{
    protected $primaryKey = 'users_id';
    protected $table = 'tb_users';

     public function deleteData(Request $request, $id){
        $bean = $this->find($id);
        $bean->delete($id);
    }
}