<?php

namespace App;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class NazhirModel extends Model
{
    protected $primaryKey = 'users_id';
    protected $table = 'nazhir';

    use SoftDeletes;
    protected $dates = ['deleted_at'];

     public function deleteData(Request $request, $id){
        $bean = $this->find($id);
        $bean->delete($id);
    }
}
