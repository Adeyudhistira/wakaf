var base_url = '//' + window.location.host + '/project-bjbs';
var dataTable;

$(function () {
    //!!!!!!!!!!!!!
    // initializing
    //!!!!!!!!!!!!!
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), "Cache-Control": "no-cache"}});
    initLib();
});

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};


function initLib() {

}
function initDropDown() {
    if ($('.ui-select').length > 0) {
        $('.ui-select').each(function () {
            var element = $(this);
            element.select2();
        });
    }
}

window.addEventListener('popstate', function (event) {
    $('.modal').modal('hide');
    $('.ui.dimmable #loader').dimmer('destroy');
    $('body .modals').remove();
    if (event.state == null) {
        window.location.href = base_url;
    } else {
        if (event.state.hasOwnProperty('url')) {
            showPage(event.state.url, true);
        }
    }
}, false);

function showError(message) {
    toastr.clear();
    toastr['error'](message, "Terjadi Kesalahan");
}

function showInfo(message) {
    toastr.clear();
    toastr['success'](message, "Berhasil");
}

function showLoading() {
    var loader = $('.ui.dimmable #mainLoader');
    loader.dimmer({closable: false}).dimmer('show');
}

function hideLoading(destroy) {
    var loader = $('.ui.dimmable #mainLoader');
    loader.dimmer({closable: false}).dimmer('hide');
    if (destroy) {
        $('.modal').modal('hide');
        $('.ui.dimmable #mainLoader').dimmer('destroy');
        $('body .modals').remove();
    }
}

function hideLoadingPartial(destroy) {
    var loader = $('.ui.dimmable #mainLoader');
    loader.dimmer({closable: false}).dimmer('hide');
    if (destroy) {
        $('.ui.dimmable #mainLoader').dimmer('destroy');
    }
}

function ajaxMenu(e, url) {
    e.preventDefault();
    showPage(url);
}
function showPage(url, skip) {
    var state = {name: "name", page: 'History', url: url};
    //subPage(url);
   showLoading();
    if (typeof skip === 'undefined') {
        window.history.pushState(state, "History", base_url + "/" + url);
        location.reload();
    } else {
        window.history.replaceState(state, "History", base_url + "/" + url);
        location.reload();
    }
}
function subPage(url) {
    showLoading();
    $.get(url, function (response, status, xhr) {
        hideLoading(true);
        var res = $.parseJSON(response);
        $('#data_content').html(res.content);
        $('#data_modal').html(res.modal);
        $('#data_script').html(res.script);
       // initLib();
    }).fail(function (response) {
        hideLoading(false);
        if (response.status === 401) {
            window.location.href = base_url;
        } else {
            showError('Terjadi Kesalahan');
        }
    });
}
function showEdit(elm, url) {
    showLoading();
    var id = $(elm).closest('tr').attr('id');
    if (id !== undefined){
        url = url + '/' + id;
    }
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        var res = $.parseJSON(response);
        $('#data_modal').html(res.content);
        $('#modalEdit').modal('show');
        initLib();
    }).fail(function (response) {
        hideLoading(false);
        if (response.status === 401) {
            window.location.href = base_url;
        } else {
            showError('Terjadi Kesalan');
        }
    });
}
function saveEdit(id, url) {
    var form = $('#formEdit'),
        formData = new FormData(form[0]);
    form.parsley().validate();
    if (!form.parsley().isValid()) {
        return false;
    } else {
        if (id !== null){
            url = url + '/' + id;
        }
        $.ajax({
            processData: false,
            contentType: false,
            type: 'POST',
            cache: false,
            url: url,
            data: formData,
            success: function (res) {
                // hideLoading(false);
                if (res.rc == 0) {
                    form[0].reset();
                    $('#modalEdit').modal('hide');
                    // dataTable.ajax.reload();
                    dataTable.draw();
                    // alert(res.rm);
                    showInfo(res.rm);
                } else {
                    showError(res.rm);
                }
            },
            error: function (data) {
                // hideLoading(false);
                $("#btnSubmit").removeClass('disabled');
                showError('Terjadi Kesalan');
            }
        });
        return false;
    }

}

function doDelete(elm, url) {
    var rowId = $(elm).closest('tr').attr('id');
    swal({
        title: "Anda Yakin?",
        text: "Data Akan Dihapus",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",
        confirmButtonText: "Submit",
        allowOutsideClick: true,
        closeOnConfirm: false
    }, function () {
        del(rowId, url);
    });
}

function del(id, url) {
    url = url + '/' + id;
    $('.modal').each(function () {
        $(this).modal('hide');
    });
    showLoading();
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        if (response.rc == 0) {
            // dataTable.ajax.reload();
            dataTable.draw();
            swal("Berhasil!", "Data Sudah Dihapus", "success");
        } else {
            swal("Gagal", response.rm, "error");
        }
    }).fail(function (response) {
        hideLoading(false);
        swal("Error", "Terjadi Kesalahan", "error");
    });
}

function clearDropdown(elm) {
    elm.html('').select2({
        data: [{id: '', text: ''}]
    })
}

function getOptRef(elm, url) {
    clearDropdown(elm);
    elm.attr('disabled', 'disable');
    getRef(url, function (response) {
        elm.removeAttr('disabled');
        if (response != null) {
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {
                    var newOption = new Option(response[i].print, response[i].value, false, false);
                    elm.append(newOption);
                }
            }
        }
    });
}
function getRef(url, callback) {
    $.get(url, function (response, status, xhr) {
        callback(response);
    }).fail(function (response) {
        showError('Terjadi Kesalahan');
        callback(null);
    });
}
function postDataCallback(formData, url, callback) {
    showLoading();
    $.ajax({
        processData: false,
        contentType: false,
        type: 'POST',
        url: url,
        data: formData,
        success: function (res) {
            hideLoading(true);
            callback(res);
        },
        error: function (data) {
            hideLoading(false);
            showError('Terjadi Kesalahan');
            callback(null);
        }
    });
}

function submitdatachange(url, data, table, modal, form) {
    showLoading();
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                modal.modal('hide');
                hideLoading(true);
                showInfo(parse.msg);
                form.reset();
            }else if(parse.code == 2){
                hideLoadingPartial(true);
                showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    }).always(function () {
        table.ajax.reload();
    });
}

function submitdata(url, data, table, modal, form) {
    showLoading();
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                modal.modal('hide');
                hideLoading(true);
                showInfo(parse.msg);
                form.reset();
            }else if(parse.code == 2){
                hideLoadingPartial(true);
                showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    }).always(function () {
      //  table.ajax.reload();
      location.reload();
    });
}
Date.prototype.getWeek = function() {
    var jan4th = new Date(this.getFullYear(),0,4);
    return Math.ceil((((this - jan4th) / 86400000) + jan4th.getDay()+1)/7);
};


var modal_upd=$('#exampleModal4');
var T_home=$('#_home');
  $("#cpasswordnew").keyup(checkPasswordMatch);
   $("#passwordnew").keyup(reset);
   
   function reset() {
    $("#divCheckPasswordMatch").html("");
    }

   function checkPasswordMatch() {
    var password = $("#passwordnew").val();
    var confirmPassword = $("#cpasswordnew").val();

    if (password != confirmPassword)
        $("#divCheckPasswordMatch").html("Passwords do not match!");
     
    else
        $("#divCheckPasswordMatch").html("Passwords match.");

}

function change(id) {
    document.getElementById("change").style.display = "block";
    $.ajax({
        url: base_url + '/user/edit/'+id,
        type: 'GET',
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {

                $('#id1').val(v.id);
                //$('#user_name1').val(v.user_name);
              $('#password1').val(v.password);
            });
            document.getElementById("change").style.display = "none";
        }
    });
   $('#exampleModal4').modal('show');
}

function processUpdate() {
    var form = $('#form_edit_lm'),
        formData = new FormData(form[0]);

    form.parsley().validate();
    if (form.parsley().isValid()) {
        document.getElementById("change").style.display = "block";
        submitdata(base_url+'/user/update1',formData,T_home,modal_upd,form[0]);
        document.getElementById("change").style.display = "none";
    }
}

