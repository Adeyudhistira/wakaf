  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var _create_form = $('#_create_form');
var _create_modal = $('#_create');
var _change_modal = $('#_change');
var modal_upd= $('#_edit');
var t_project = $('#t_project').DataTable({
    width:100,
    scrollX: true,

});
function tambah() {
    _create_modal.modal('show');
}
function processInsert() {


_create_form.parsley().validate();
    var _form_data = new FormData(_create_form[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
   if(_create_form.parsley().isValid()){
       // submitdata('Project',_form_data,t_project,_create_modal,_create_form[0]);

       $('.loading-mail').delay(0).fadeIn('slow');

     $.ajax({
        type: 'POST',
        url: 'Project',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            // var parse = $.parseJSON(res);
            // if (parse.code == 1) {
            //     _create_modal.modal('hide');
            //     //hideLoading(true);
            //     showInfo(parse.msg);
            //     _create_form[0].reset();
            // }else if(parse.code == 2){
            //     //hideLoadingPartial(true);
            //     //showError(parse.msg);
            //     _create_modal.modal('hide');
            //     swal("Info!", "Task Untuk "+ parse.msg +" Sudah Ada", "info");
            //     _create_form[0].reset();
            // }else {
            //     var li = '<ul>';
            //     $.each(parse.data, function (i, v) {
            //         li += '<li>' + v[0] + '</li>';
            //     });
            //     li += '</ul>';
            //     hideLoadingPartial(true);
            //     showError(li);
            // }
        }
    }).always(function () {
        var loader = $('.ui.dimmable #mainLoader');
         loader.dimmer({closable: false}).dimmer('show');
       location.reload();
    });

    }

}
function editshow(id) {

$.ajax({
        url: 'project/edit/'+id,
        type: 'GET',
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {

              var bilangan = v.target_dana;

              var	number_string = bilangan.toString(),
              	sisa 	= number_string.length % 3,
              	rupiah 	= number_string.substr(0, sisa),
              	ribuan 	= number_string.substr(sisa).match(/\d{3}/g);

              if (ribuan) {
              	separator = sisa ? ',' : '';
              	rupiah += separator + ribuan.join(',');
              }

              var url = "http://localhost/wakaf/uploadimg/";

              // var dana = convertToRupiah(v.target_dana);
              // console.log(v.target_dana);
                //console.log(v.crt_dt);

                $('#id').val(v.project_id);
                $('#status_project1').val(v.status_project);
                $('#jenis_wakaf1').val(v.jenis_wakaf);
                $('#city_id1').val(v.city_id);
                $('#prov_id1').val(v.prov_id);
                $('#end_date1').val(v.end_date);
                $('#crt_dt1').val(v.crt_dt);
                $('#target_dana1').val(rupiah);
                $('#category_id1').val(v.category_id);
                $('#deskripsi_project1').val(v.deskripsi_project);
                $('#nama_project1').val(v.nama_project);
                $('#users_id1').val(v.users_id);

                $("#logo_lama").attr("src",url+v.main_img);
                $('#data_logo_lama').val(v.main_img);

                $("#gp_lama1").attr("src",url+v.img1);
                $('#data_gp_lama1').val(v.img1);

                $("#gp_lama2").attr("src",url+v.img2);
                $('#data_gp_lama2').val(v.img2);

                $("#gp_lama3").attr("src",url+v.img3);
                $('#data_gp_lama3').val(v.img3);

                var data_city = v.city_id;

                console.log(data_city);

                $.ajax({
                    type: 'GET',
                    url: base_url+'/select/kab/'+ v.prov_id,
                    success: function (res) {
                      var data = $.parseJSON(res);
                      _items = "<option value=''>Pilih Kota/Kabupaten</option>";
                      $.each(data, function (k,v) {
                         _items += "<option value='"+v.city_id+"'>"+v.city_name+"</option>";
                      });
                      $('#city_id1').html(_items);
                      $('#city_id1').val(data_city);

                    }
                });


            });
        }
    });
$('#_edit').modal('show');
}

function processUpdate() {
    var form = $('#form_edit_lm'),
        formData = new FormData(form[0]);

   // form.parsley().validate();
     //alert('ab');
    //if (form.parsley().isValid()) {
      //  alert('a');
        submitdata('project/update',formData,t_project,modal_upd,form[0]);

    //}


}
function hapus(id,url) {

    swal({
            title: "Anda Yakin?",
            text: "Data Akan Dihapus!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                  del(id,url);
              } else {
                swal("Cancelled", "Your imaginary file is safe", "error");
            }
        });
}

function del(id,url) {
    url = url + '/' + id;
    $('.modal').each(function () {
        $(this).modal('hide');
    });
    showLoading();
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        if (response.rc == 0) {
            // dataTable.ajax.reload();
            //t_project.draw();
            swal("Berhasil!", "Data Sudah Dihapus", "success");
             var loader = $('.ui.dimmable #mainLoader');
             loader.dimmer({closable: false}).dimmer('show');
             location.reload();
        } else {
            swal("Gagal", response.rm, "error");
        }
    }).fail(function (response) {
        hideLoading(false);
        swal("Error", "Terjadi Kesalahan", "error");
    });
}

  $('#prov_id').on('change', function (v) {
    var _items = "";
    $.ajax({
            type: 'GET',
            url: base_url+'/select/kab/'+ this.value,
            success: function (res) {
              var data = $.parseJSON(res);
              _items = "<option value=''>Pilih Kota/Kabupaten</option>";
              $.each(data, function (k,v) {
                 _items += "<option value='"+v.city_id+"'>"+v.city_name+"</option>";
              });
              $('#city_id').html(_items);
            }
        });
});

  $('#prov_id1').on('change', function (v) {
    var _items = "";
    $.ajax({
            type: 'GET',
            url: base_url+'/select/kab/'+ this.value,
            success: function (res) {
              var data = $.parseJSON(res);
              _items = "<option value=''>Pilih Kota/Kabupaten</option>";
              $.each(data, function (k,v) {
                 _items += "<option value='"+v.city_id+"'>"+v.city_name+"</option>";
              });
              $('#city_id1').html(_items);
            }
        });
});

function detail(id) {
    //$('#id').val(id);
    window.location.href = base_url +'/project/detail/' + id;
    //_create_modal.modal('show');
}
