  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var _create_form = $('#_create_form');
var _create_modal = $('#_create');
var _change_modal = $('#_change');
var modal_upd= $('#_edit');
var t_province = $('#t_province').DataTable({
    width:100,
    scrollX: true,

});
function tambah() {
    _create_modal.modal('show');
}
function processInsert() {
 _create_form.parsley().validate();
    var _form_data = new FormData(_create_form[0]);

    // for (var pair of _form_data.entries()) {
    //     console.log(pair[0]+ ', ' + pair[1]);
    // }
    if(_create_form.parsley().isValid()){
        submitdata('Province',_form_data,t_province,_create_modal,_create_form[0]);
    }

}
function editshow(id) {

$.ajax({
        url: 'province/edit/'+id,
        type: 'GET',
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {

                $('#id').val(v.prov_id);
                $('#prov_name1').val(v.prov_name);


            });
        }
    });
$('#_edit').modal('show');
}

function processUpdate() {
    var form = $('#form_edit_lm'),
        formData = new FormData(form[0]);

    form.parsley().validate();
     //alert('ab');
    if (form.parsley().isValid()) {
      //  alert('a');
        submitdata('province/update',formData,t_province,modal_upd,form[0]);
    }
}
function hapus(id,url) {

    swal({
            title: "Anda Yakin?",
            text: "Data Akan Dihapus!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                  del(id,url);
              } else {
                swal("Cancelled", "Your imaginary file is safe", "error");
            }
        });
}

function del(id,url) {
    url = url + '/' + id;
    $('.modal').each(function () {
        $(this).modal('hide');
    });
    showLoading();
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        if (response.rc == 0) {
            // dataTable.ajax.reload();
            //t_province.draw();
            swal("Berhasil!", "Data Sudah Dihapus", "success");
            location.reload();
        } else {
            swal("Gagal", response.rm, "error");
        }
    }).fail(function (response) {
        hideLoading(false);
        swal("Error", "Terjadi Kesalahan", "error");
    });
}
