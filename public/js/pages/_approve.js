var _rating_modal = $('#_rating');
var _return_modal = $('#_return');
var _return_form = $('#_return_form');

$.fn.raty.defaults.path = base_url +'/images/raty/';
$('#rate').raty({
    click: function (v) {
        $.ajax({
            type: 'GET',
            url: '',
            success: function (res) {
                $.ajax({
                    type: 'POST',
                    url: base_url + '/approve/rate',
                    data: {
                        rate: v,
                        id_subtask: $('#id_subtask').val(),
                        end_date: $('#end_date1').val(),
                        duration: $('#duration1').val()
                    },
                    success: function (res) {
                        console.log(res);
                    }
                });
            }
        }).fail(function () {
            alert('terjadi kesalahan');
        });
        //console.log(v);
    }
});
var t_approve = $('#t_project').DataTable({
    processing: true,
    serverSide: true,
    scrollX: true,
    ajax: {
        url: 'approve/table'
    },
    columns: [
        {data: 'id', name: 'id'},
        {data: 'project_name', name: 'project_name', width: "300px"},
        {data: 'task_name', name: 'task_name', width: "100px"},
        {data: 'sub_task_name', name: 'sub_task_name', width: "100px"},
        {data: 'start_date', name: 'start_date'},
        {data: 'end_date', name: 'end_date'},
        {data: 'target_date', name: 'target_date'},
        {data: 'duration', name: 'duration', width: "50px"},
        {data: 'pic', name: 'pic'},
        {data: 'action', name: 'action', sortable: false, searchable: false, width: "150px"}
    ]
});

function rating(id,tgl) {
    $('#id_subtask').val(id);
     var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();

                if(dd<10) {
                    dd = '0'+dd
                } 

                if(mm<10) {
                    mm = '0'+mm
                } 

                today = yyyy + '-' + mm + '-' + dd;


                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date(tgl);
                var secondDate = new Date(today);
                

               var wd = workingDaysBetweenDates(firstDate,secondDate);
             
                $('#end_date1').val(tgl);
                $('#duration1').val(wd);
               function workingDaysBetweenDates(startDate, endDate) {
  
                    // Validate input
                    //if (endDate < startDate)
                      //  return 0;
                    
                    // Calculate days between dates
                    var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
                    startDate.setHours(0,0,0,1);  // Start just after midnight
                    endDate.setHours(23,59,59,999);  // End just before midnight
                    var diff = endDate - startDate;  // Milliseconds between datetime objects    
                    var days = Math.ceil(diff / millisecondsPerDay);
                    
                    // Subtract two weekend days for every week in between
                    var weeks = Math.floor(days / 7);
                    days = days - (weeks * 2);

                    // Handle special cases
                    var startDay = startDate.getDay();
                    var endDay = endDate.getDay();
                    
                    // Remove weekend not previously removed.   
                    if (startDay - endDay > 1)         
                        days = days - 2;      
                    
                    // Remove start day if span starts on Sunday but ends before Saturday
                    if (startDay == 0 && endDay != 6)
                        days = days - 1  
                            
                    // Remove end day if span ends on Saturday but starts after Sunday
                    if (endDay == 6 && startDay != 0)
                        days = days - 1  
                    
                    return days;
                }  
    _rating_modal.modal('show');
}

function refreshPage() {
    var loader = $('.ui.dimmable #mainLoader');    
    loader.dimmer({closable: false}).dimmer('show');
    location.reload();
    loader.dimmer({closable: false}).dimmer('hide');
}

function showReturn(id) {
    $('#id_subtask_return').val(id);
    _return_modal.modal('show');
}

function processReturn() {
    _return_form.parsley().validate();
    var _form_data = new FormData(_return_form[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
    if(_return_form.parsley().isValid()) {

        $.ajax({
            type: 'POST',
            url: base_url+'/approve/rollback',
            data: _form_data,
            processData: false,
            contentType: false,
            success: function (res) {
                var parse = $.parseJSON(res);
                if (parse.code == 1) {
                    _return_modal.modal('hide');
                    hideLoading(true);
                    showInfo(parse.msg);
                    _return_form[0].reset();
                } else if (parse.code == 2) {
                    hideLoadingPartial(true);
                    showError(parse.msg);
                } else {
                    var li = '<ul>';
                    $.each(parse.data, function (i, v) {
                        li += '<li>' + v[0] + '</li>';
                    });
                    li += '</ul>';
                    hideLoadingPartial(true);
                    showError(li);
                }
            }
        }).always(function () {
            var loader = $('.ui.dimmable #mainLoader');
            loader.dimmer({closable: false}).dimmer('show');
            location.reload();
        });
    }
}
