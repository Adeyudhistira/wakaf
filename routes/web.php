<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => 'auth'], function () {

Route::get('/home','HomeController@index')->name('home');
Route::get( '/', ['as'=>'home', 'uses'=> 'HomeController@index']);


});

Route::resource('Category','CategoryController');
Route::prefix('category')->group(function (){
        Route::get('edit/{id}','CategoryController@findRealById');
        Route::post('update', 'CategoryController@update');
        Route::get('delete/{id}', 'CategoryController@delete');
    });

Route::resource('Province','ProvinceController');
Route::prefix('province')->group(function (){
        Route::get('edit/{id}','ProvinceController@findRealById');
        Route::post('update', 'ProvinceController@update');
        Route::get('delete/{id}', 'ProvinceController@delete');
    });

Route::resource('Status','StatusController');
Route::prefix('status')->group(function (){
        Route::get('edit/{id}','StatusController@findRealById');
        Route::post('update', 'StatusController@update');
        Route::get('delete/{id}', 'StatusController@delete');
    });

Route::resource('Branch','BranchController');
Route::prefix('branch')->group(function (){
        Route::get('edit/{id}','BranchController@findRealById');
        Route::post('update', 'BranchController@update');
        Route::get('delete/{id}', 'BranchController@delete');
    });

Route::resource('City','CityController');
Route::prefix('city')->group(function (){
        Route::get('edit/{id}','CityController@findRealById');
        Route::post('update', 'CityController@update');
        Route::get('delete/{id}', 'CityController@delete');
    });

Route::resource('Project','ProjectController');
Route::prefix('project')->group(function (){
        Route::get('edit/{id}','ProjectController@findRealById');
        Route::post('update', 'ProjectController@update');
        Route::get('delete/{id}', 'ProjectController@delete');
        Route::get('detail/{id}', 'ProjectController@detail');
    });



Route::resource('Nazhir','NazhirController');

Route::prefix('nazhir')->group(function (){
        Route::get('table', 'NazhirController@table');
        Route::get('edit/{id}','NazhirController@findRealById');
        Route::get('delete/{id}', 'NazhirController@delete');
        Route::post('update', 'NazhirController@update');

    });

Route::prefix('select')->group(function (){
        Route::get('kab/{id}', 'HomeController@kab');
    });

Route::prefix('chart')->group(function (){
        Route::get('utama', 'HomeController@utama');
        Route::get('bar1', 'HomeController@bar1');
        Route::get('bar2', 'HomeController@bar2');
        Route::get('bar3', 'HomeController@bar3');
    });

Route::resource('Laporan','LaporanController');
Route::prefix('laporan')->group(function (){
        Route::get('transaksi', 'LaporanController@transaksi');
    });
