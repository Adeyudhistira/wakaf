@section('pageTitle', 'Dashboard')
@section('content')      
      <div class="content-body">
        <!-- Analytics charts -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-12">
            <div class="card">
              <div class="card-header no-border-bottom bg-success">
                <h4 class="card-title white">Visitors Overview</h4>
                <a class="heading-elements-toggle"><i class="ft-more-horizontal font-medium-3"></i></a>
              
              </div>
              <div class="card-content">
                <div class="card-body">
                  <div class="row my-1">
                    <div class="col-lg-4 col-12 p-1 border-right-blue-grey border-right-lighten-5" style="background-color: #FF5722">
                      <div class="text-center">
                        <h3 class="white">{{$totalnazhir[0]->value}}</h3>
                        <p class="text-muted white">Total Nazhir
                          
                        </p>
                        <div id="sp-bar-total-cost1"></div>
                      </div>
                    </div>
                    <div class="col-lg-4 col-12 p-1 border-right-blue-grey border-right-lighten-5" style="background-color: #4169E1">
                      <div class="text-center">
                        <h3 class="white">{{$totalproject[0]->value}}</h3>
                        <p class="text-muted white">Total Project
                         
                        </p>
                        <div id="sp-bar-total-cost2"></div>
                      </div>
                    </div>
                    <div class="col-lg-4 col-12 p-1 bg-success">
                      <div class="text-center">
                        <h3 class="white">{{number_format($totaldana[0]->value)}}</h3>
                        <p class="text-muted white">Total Dana Wakaf
                          
                        </p>
                        <div id="sp-bar-total-cost3"></div>
                      </div>
                    </div>
                  </div>
                  <ul class="list-inline text-center mt-3">
                    
                    
                      @foreach($ket as $item)
                     <li>
                      <h6><i class="ft-circle pl-1" style="color: {{$item->colour}}"></i> {{$item->category_desc}}</h6>
                      </li>
                      @endforeach
                      
                    
                  </ul>
                  <div class="chartjs">
                    <canvas id="visitors-graph1" height="275"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="card">
              <div class="card-header bg-success">
                <h4 class="card-title white">Log Transaksi Wakaf</h4>
                <a class="heading-elements-toggle"><i class="ft-more-horizontal font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content">
                <div class="card-body">
                  <p>Jumlah Wakaf Terbayar {{$total[0]->paid}}, Jumlah Wakaf belum Realisasi {{$total[0]->wait}}
                   
                  </p>
                </div>
                <div class="table-responsive">
                  <table class="table table-hover table-striped mb-0" width="100%" id="tbl">
                    <thead>
                      <tr>
                        <th>No VA</th>
                        <th>Nama Project</th>
                        <th>Status</th>
                        <th>Created Date</th>
                        <th>Nominal Wakaf</th>
                        <th style="display: none">tgl</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($data as $item)
                      <tr>
                        <td>{{$item->no_va}}</td>
                        <td>{{$item->nama_project}}</td>
                        <td>{{$item->status}}</td>
                        <td>{{ date('d M Y H:i:s',strtotime($item->crt_dt)) }}</td>
                        <td class="text-right">{{ number_format($item->nominal) }}</td>
                        <td style="display: none">{{$item->crt_dt}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>      

      </div>
@stop 
@section('script') 
<script type="text/javascript">
  var table = $('#tbl').DataTable({
    width:100,
    scrollX: true,
    searching: false,
    "order": [[ 5, "desc" ]], //or asc 
    
});



$.ajax({
    type: 'GET',
    url: 'chart/bar1',
    success: function (res) {
        var data = $.parseJSON(res);
        var bln1,bln2,bln3,bln4,bln5,bln6,bln7,bln8,bln9,bln10,bln11,bln12;
        //var jmltx_10,jmltx_20;
        //var total=[];

        $.each(data, function (k,v) {
            
            bln1=v.bulan_1;
            bln2=v.bulan_2;
            bln3=v.bulan_3;
            bln4=v.bulan_4;
            bln5=v.bulan_5;
            bln6=v.bulan_6;
            bln7=v.bulan_7;
            bln8=v.bulan_8;
            bln9=v.bulan_9;
            bln10=v.bulan_10;
            bln11=v.bulan_11;
            bln12=v.bulan_12;
        });

        $("#sp-bar-total-cost1").sparkline([bln1,bln2,bln3,bln4,bln5,bln6,bln7,bln8,bln9,bln10,bln11,bln12], {
        type: 'bar',
        width: '100%',
        height: '30px',
        barWidth: 2,
        barSpacing: 4,
        barColor: '#FFFFFF'
      });

   


     }
});   

$.ajax({
    type: 'GET',
    url: 'chart/bar2',
    success: function (res) {
        var data = $.parseJSON(res);
        var bln1,bln2,bln3,bln4,bln5,bln6,bln7,bln8,bln9,bln10,bln11,bln12;
        //var jmltx_10,jmltx_20;
        //var total=[];

        $.each(data, function (k,v) {
            
            bln1=v.bulan_1;
            bln2=v.bulan_2;
            bln3=v.bulan_3;
            bln4=v.bulan_4;
            bln5=v.bulan_5;
            bln6=v.bulan_6;
            bln7=v.bulan_7;
            bln8=v.bulan_8;
            bln9=v.bulan_9;
            bln10=v.bulan_10;
            bln11=v.bulan_11;
            bln12=v.bulan_12;
        });

        $("#sp-bar-total-cost2").sparkline([bln1,bln2,bln3,bln4,bln5,bln6,bln7,bln8,bln9,bln10,bln11,bln12], {
        type: 'bar',
        width: '100%',
        height: '30px',
        barWidth: 2,
        barSpacing: 4,
        barColor: '#FFFFFF'
      });

   


     }
});   


$.ajax({
    type: 'GET',
    url: 'chart/bar3',
    success: function (res) {
        var data = $.parseJSON(res);
        var bln1,bln2,bln3,bln4,bln5,bln6,bln7,bln8,bln9,bln10,bln11,bln12;
        //var jmltx_10,jmltx_20;
        //var total=[];

        $.each(data, function (k,v) {
            
            bln1=v.bulan_1;
            bln2=v.bulan_2;
            bln3=v.bulan_3;
            bln4=v.bulan_4;
            bln5=v.bulan_5;
            bln6=v.bulan_6;
            bln7=v.bulan_7;
            bln8=v.bulan_8;
            bln9=v.bulan_9;
            bln10=v.bulan_10;
            bln11=v.bulan_11;
            bln12=v.bulan_12;
        });

        $("#sp-bar-total-cost3").sparkline([bln1,bln2,bln3,bln4,bln5,bln6,bln7,bln8,bln9,bln10,bln11,bln12], {
        type: 'bar',
        width: '100%',
        height: '30px',
        barWidth: 2,
        barSpacing: 4,
        barColor: '#FFFFFF'
      });

   


     }
});   


$.ajax({
    type: 'GET',
    url: 'chart/utama',
    success: function (res) {
        var data = $.parseJSON(res);
        //var bln1,bln2,bln3,bln4,bln5,bln6,bln7,bln8,bln9,bln10;
        //var jmltx_10,jmltx_20;
        var bln1,bln2,bln3,bln4,bln5,bln6,bln7,bln8,bln9,bln10,bln11,bln12,kategori;
        var array=[];

        $.each(data, function (k,v) {
            
            array.push({
          
            label: v.category_desc,
            data: [v.bulan_1,v.bulan_2,v.bulan_3,v.bulan_4,v.bulan_5,v.bulan_6,v.bulan_7,v.bulan_8,v.bulan_9,v.bulan_10,v.bulan_11,v.bulan_12],
            fill: false,
            borderColor: v.colour,
            pointBorderColor: v.colour,
            pointBackgroundColor: v.colour,
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
          });
            
        });
      
var ctx = $("#visitors-graph1");

    // Chart Options
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        hover: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                }
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                ticks: {
                    display: true,
                    maxTicksLimit: 5
                }
            }]
        },
        title: {
            display: false,
        }
    };

    // Chart Data
    var chartData = {
        labels: ['jan','Feb','Mar','Apr','Mei','Jun','Jul','Aguts','Sep','Okt','Nov','Des'],
        datasets: array
    };

    var config = {
        type: 'line',

        // Chart Options
        options : chartOptions,

        data : chartData
    };

    // Create the chart
    var lineChart = new Chart(ctx, config);  




      } 
  });        

 
</script>
@stop    