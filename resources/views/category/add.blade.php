<div class="modal animated slideInRight text-left" id="_create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
        <h4 class="modal-title white" id="myModalLabel76">Tambah Category</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
  <div class="modal-body">
    <form class="form" id="_create_form">
      <div class="form-body">
        <h4 class="form-section"><i class="fa fa-desktop"></i> Data Category</h4>
          <div class="row">
            <div class="form-group col-md-12 mb-2">
              <label for="category_desc">Category</label>
              <input id="category_desc" class="form-control border-success " placeholder="Category" name="category_desc" type="text" data-parsley-required>
            </div> 
          </div>
      </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
    <button type="button" class="btn btn-outline-success"  onclick="processInsert()">Simpan</button>
  </div>
</div>
</div>
</div>
