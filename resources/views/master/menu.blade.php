<div class="navbar-container main-menu-content container center-layout" data-menu="menu-container">
      <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
        <li class="dropdown nav-item" data-menu="dropdown">
          <a class="dropdown-toggle nav-link white" href="#" onclick="ajaxMenu(event,'home')" data-toggle="dropdown"><i class="icon-home"></i>
            <span data-i18n="nav.dash.main">Dashboard</span>
          </a>
        </li>
        <li class="dropdown nav-item" data-menu="dropdown">
          <a class="dropdown-toggle nav-link white" href="index.html" data-toggle="dropdown"><i class="icon-briefcase"></i>
            <span data-i18n="nav.dash.main">Data Master</span>
          </a>
          <ul class="dropdown-menu">
            <li data-menu=""><a class="dropdown-item" href="#" onclick="ajaxMenu(event,'Nazhir')" data-toggle="dropdown">Data Nazhir</a>
            </li>
            <li data-menu=""><a class="dropdown-item" href="#" onclick="ajaxMenu(event,'Project')" data-toggle="dropdown">Data Project</a>
            </li>
          </ul>
        </li>

        <li class="dropdown nav-item" data-menu="dropdown">
          <a class="dropdown-toggle nav-link white" href="index.html" data-toggle="dropdown"><i class="icon-notebook"></i>
            <span data-i18n="nav.dash.main">Data Referensi</span>
          </a>
          <ul class="dropdown-menu">
            <li data-menu=""><a class="dropdown-item" href="#" onclick="ajaxMenu(event,'Category')" data-toggle="dropdown">Data Category</a>
            </li>
            <li data-menu=""><a class="dropdown-item" href="#" onclick="ajaxMenu(event,'City')" data-toggle="dropdown">Data City</a>
            </li>
            <li data-menu=""><a class="dropdown-item" href="#" onclick="ajaxMenu(event,'Province')" data-toggle="dropdown">Data Province</a>
            </li>
            <li data-menu=""><a class="dropdown-item" href="#" onclick="ajaxMenu(event,'Status')" data-toggle="dropdown">Data Status</a>
            </li>
            <li data-menu=""><a class="dropdown-item" href="#" onclick="ajaxMenu(event,'Branch')" data-toggle="dropdown">Data Branch</a>
            </li>
          </ul>
        </li>

        <li class="dropdown nav-item" data-menu="dropdown">
          <a class="dropdown-toggle nav-link white" href="index.html" data-toggle="dropdown"><i class="icon-printer"></i>
            <span data-i18n="nav.dash.main">Laporan</span>
          </a>
          <ul class="dropdown-menu">
            <li data-menu=""><a class="dropdown-item" href="#" onclick="ajaxMenu(event,'laporan/transaksi')" data-toggle="dropdown">Laporan Transaksi</a>
            </li>
          </ul>
        </li>

      </ul>
    </div>