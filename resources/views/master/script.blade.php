
 <!-- BEGIN VENDOR JS-->
  <script src="{{ asset('vendors/js/vendors.min.js') }}" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script type="text/javascript" src="{{ asset('vendors/js/ui/jquery.sticky.js') }}"></script>
  <script src="{{ asset('vendors/js/extensions/jquery.knob.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/scripts/extensions/knob.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendors/js/charts/raphael-min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendors/js/charts/morris.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js') }}"
  type="text/javascript"></script>
  <script src="{{ asset('vendors/js/charts/jvector/jquery-jvectormap-world-mill.js') }}"
  type="text/javascript"></script>
  <script src="{{ asset('data/jvector/visitor-data.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendors/js/charts/chart.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendors/js/charts/jquery.sparkline.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('vendors/js/extensions/unslider-min.js') }}" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN ROBUST JS-->
  <script src="{{ asset('js/core/app-menu.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/core/app.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/scripts/customizer.js') }}" type="text/javascript"></script>
  <!-- END ROBUST JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{ asset('js/scripts/pages/dashboard-analytics.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/_global.js') }}" type="text/javascript"></script>
  <!--Palette JS-->
<script src="{{ asset('js/dimmer.min.js') }}"></script>
  <!-- END PAGE LEVEL JS-->
   <!-- BEGIN PAGE VENDOR JS-->
   <script src="{{ asset('vendors/js/tables/datatable/datatables.min.js') }}"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{ asset('js/scripts/tables/datatables-extensions/datatables-sources.js') }}"></script>
 


  <script src="{{ asset('vendors/js/extensions/toastr.min.js') }}" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
 
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{ asset('js/scripts/extensions/toastr.js') }}" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->

  <script src="{{ asset('vendors/js/extensions/sweetalert.min.js') }}" type="text/javascript"></script>
 
  <script src="{{ asset('js/scripts/extensions/sweet-alerts.js') }}" type="text/javascript"></script>

  <!-- Parsley Validation-->
<script src="{{ asset('assets/vendor/parsley/parsley.min.js') }}"></script>
<script src="{{ asset('assets/vendor/parsley/id.js') }}"></script>