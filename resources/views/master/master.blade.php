<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <html class="loading" lang="en" data-textdirection="ltr">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="author" content="PIXINVENT">
  <title>@yield('pageTitle') - Wakaf</title>
  <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.png">
  <link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/ico/favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,400,500,700"
  rel="stylesheet">
  @include('master.head')

  <style media="screen">
      /* body{
      background:url('http://localhost/wakaf/images/bg_web.jpg');
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-size:cover;
      filter: alpha(opacity=10);
    } */
    #background {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background:url('http://localhost/wakaf/images/bg_web.jpg');
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-size: 100%;
      opacity: 1;
      filter:alpha(opacity=1);
    }
  </style>
</head>
<body class="horizontal-layout horizontal-menu horizontal-menu-padding 2-columns   menu-expanded"
data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
<div id="background"></div>
  <!-- fixed-top-->
 @include('master.header')

<div class="bg-success header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow"
  role="navigation" data-menu="menu-wrapper">
  @include('master.menu')

</div>
<div class="bg_app">
</div>
  <div class="app-content container center-layout mt-2">
    <div class="content-wrapper ui dimmable">
      <div class="content-header row">
      </div>
        <div class="ui dimmer" id="mainLoader">
            <div class="ui loader"></div>
        </div>
        <div id="data_content">
            @yield('content')
        </div>
    </div>
  </div>
 <footer class="footer fixed-bottom footer-static footer-light navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2 container center-layout">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent"
        target="_blank">TEAM BASYS </a>, All rights reserved. </span>
      <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block"></span>
    </p>
  </footer>
<div id="data_modal">@yield('modal')</div>

    @include('master.script')
<div id="data_script">@yield('script')</div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
</body>

</html>
