<!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/vendors.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/charts/jquery-jvectormap-2.0.3.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/charts/morris.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/extensions/unslider.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/weather-icons/climacons.min.css') }}">
  <!-- END VENDOR CSS-->
  <!-- BEGIN ROBUST CSS-->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
  <!-- END ROBUST CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/core/menu/menu-types/horizontal-menu.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/core/colors/palette-gradient.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/calendars/clndr.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/core/colors/palette-climacon.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/pages/users.css') }}">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<link href="{{ asset('css/dimmer.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/loader.min.css') }}" rel="stylesheet">
  <!-- END Custom CSS-->


<!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/extensions/toastr.css') }}">
  <!-- END VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins/extensions/toastr.css') }}">
  <!-- END Page Level CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/tables/datatable/datatables.min.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/extensions/sweetalert.css') }}">