@section('pageTitle', 'Status')
@section('content')
 <div class="content-body">
 <div class="row">
     <div class="col-12">
   
        <a href="#" onclick="tambah()" class="btn btn-social bg-success width-200 mr-1 mb-1 btn-dropbox">
                            <span class="fa fa-plus font-medium-3"></span> Tambah Status</a>
        <div class="card">
          <div class="card-header">
                <h4 class="card-title">Data Status</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
        <div class="card-content collpase show">
            <div class="card-body card-dashboard">
                  
                    <table id="t_status" width="100%" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Definition</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $item)
                        <tr>
                          <td>{{$item->nomor_urut}}</td>
                          <td>{{$item->definition}}</td>
                          <td><button type="button" class="btn btn-info bg-blue" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" onclick="editshow({{$item->id}})">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-danger bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus" onclick="hapus({{$item->id}},base_url+'/status/delete');">
                        <i class="fa fa-trash"></i>
                    </button></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@include('status.add')
@include('status.edit')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('assets/js/_status.js') }}"></script>
@stop