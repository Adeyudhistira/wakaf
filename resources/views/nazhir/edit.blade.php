<div class="modal animated slideInRight text-left" id="_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
        <h4 class="modal-title white" id="myModalLabel76">Edit Nazhir</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
  <div class="modal-body">
    <form class="form" id="form_edit_lm">
      <div class="form-body">
        <h4 class="form-section"><i class="fa fa-desktop"></i> Data Nazhir</h4>
          <div class="row">
            <div class="form-group col-md-6 mb-2">
              <label for="comp_name">Nama Nazhir</label>
              <input id="comp_name1" required class="form-control border-success " placeholder="Nama Nazhir" name="comp_name" type="text">
              <input id="id" class="form-control border-success " name="id" type="hidden">
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="alamat">Alamat</label>
              <input id="alamat1" required class="form-control border-success" placeholder="Alamat" name="alamat" type="text">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6 mb-2">
              <label for="comp_desc">Deskripsi Nazhir</label>
              <textarea id="comp_desc1" required class="form-control border-success" placeholder="Deskripsi Nazhir" name="comp_desc"></textarea>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="about">Tentang Nazhir</label>
              <textarea id="about1" required class="form-control border-success" placeholder="Tentang Nazhir" name="about"></textarea>
            </div>
          </div>
          <div class="row">
          @php
          $data = \DB::select("SELECT * FROM reff_province");
          @endphp
            <div class="form-group col-md-6 mb-2">
              <label for="prov_id">Provinsi</label>
              <select id="prov_id1" required class="form-control border-success" name="prov_id">
                <option value="">Pilih Provinsi</option>
                @foreach($data as $item)
                <option value="{{$item->prov_id}}">{{$item->prov_name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="city_id">Kota / Kabupaten</label>
              <select id="city_id1" required class="form-control border-success" name="city_id">
                <option value="">Pilih Kota/Kabupaten</option>
              </select>

            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="logo">Logo Nazhir</label>
              <img id="logo_lama" width="100" style="display:block;border:1px solid #e8e8e8;" src="" alt="">
              <input id="data_logo_lama" type="hidden" name="logo_lama" value="">
            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="logo">Upload Logo Baru</label>
              <input id="logo" class="form-control border-success" placeholder="Logo" name="logo" type="file" value="D:/bg_wakaf.jpg" >
            </div>
          </div>
          <h4 class="form-section"><i class="fa fa-user"></i> Data Admin Nazhir</h4>
          <div class="row">
            <div class="form-group col-12 mb-2">
              <label for="userinput5">Nama Admin</label>
              <input required class="form-control border-success" placeholder="Admin" id="admin_name1" name="admin_name" type="text">
            </div>
          </div>
        <div class="row">
          <div class="form-group col-12 mb-2">
            <label for="userinput6">Alamat Email</label>
            <input required class="form-control border-success" placeholder="Email" id="email1" name="email" type="email">
          </div>
        </div>
        <div class="row">
          <div class="form-group col-12 mb-2">
            <label>No Telp</label>
            <input required class="form-control border-success" placeholder="Contact Number" id="phone_no1" min=0 minlength="8" maxlength="14" name="phone_no" type="number">
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
    <button type="button" class="btn btn-outline-success"  onclick="processUpdate()">Simpan</button>
  </div>
</div>
</div>
</div>
