@section('pageTitle', 'Nazhir')
@section('content')
 <div class="content-body">
 <div class="row">
     <div class="col-12">

        <a href="#" onclick="tambah()" class="btn btn-social bg-success width-200 mr-1 mb-1 btn-dropbox">
                            <span class="fa fa-plus font-medium-3"></span> Tambah Nazhir</a>
        <div class="card">
          <div class="card-header bg-success">
                <h4 class="card-title white">Data Nazhir</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements white">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
        <div class="card-content collpase show">
            <div class="card-body card-dashboard">

                    <table id="t_nazhir" class="table table-striped mb-0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Logo</th>
                          <th>Nama Nazhir</th>
                          <th>Alamat</th>
                          <th>Tentang Nazhir</th>
                          <th>Provinsi</th>
                          <th>Kota / Kabupaten</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $item)
                        <tr>
                          <td>{{$item->nomor_urut}}</td>
                           <td>
                            @if($item->logo)
                            <figure class="card card-img-top border-grey border-lighten-2" itemprop="associatedMedia"
                              itemscope itemtype="http://schema.org/ImageObject">
                                <a href="{{asset('/uploadimg/')}}/{{ $item->logo }}"  itemprop="contentUrl" data-size="280x160">
                                  <img class="gallery-thumbnail card-img-top" src="{{asset('/uploadimg/')}}/{{ $item->logo }}"
                                  itemprop="thumbnail" alt="Image description" />
                                </a>
                              </figure>
                              @else
                              <figure class="card card-img-top border-grey border-lighten-2" itemprop="associatedMedia"
                              itemscope itemtype="http://schema.org/ImageObject">
                                <a href="{{asset('images/gallery/1.jpg')}}" itemprop="contentUrl" data-size="280x160">
                                  <img class="gallery-thumbnail card-img-top" src="{{asset('images/gallery/1.jpg')}}"
                                  itemprop="thumbnail" alt="Image description" />
                                </a>
                              </figure>
                              @endif
                          </td>
                          <td>{{$item->comp_name}}</td>
                          <td>{{$item->alamat}}</td>
                          <td>{{ isset($item->comp_desc) ?  substr($item->comp_desc,0,30) :'-'}} {{ (strlen($item->comp_desc) >= 30) ? "..." :"" }} </td>
                          <td>{{$item->prov_name}}</td>
                          <td>{{$item->city_name}}</td>
                          <td nowrap><button type="button" class="btn btn-info bg-blue" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" onclick="editshow({{$item->users_id}})">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-danger bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus" onclick="hapus({{$item->users_id}},'nazhir/delete')">
                        <i class="fa fa-trash"></i>
                    </button></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@include('nazhir.add')
@include('nazhir.edit')
@endsection
@section('script')
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var _create_form = $('#_create_form');
var _create_modal = $('#_create');
var _change_modal = $('#_change');
var modal_upd= $('#_edit');
var t_nazhir = $('#t_nazhir').DataTable({
    width:100,
    scrollX: true,

});
function tambah() {
    _create_modal.modal('show');
}
  $('#prov_id').on('change', function (v) {
    var _items = "";
    $.ajax({
            type: 'GET',
            url: base_url+'/select/kab/'+ this.value,
            success: function (res) {
              var data = $.parseJSON(res);
              _items = "<option value=''>Pilih Kota/Kabupaten</option>";
              $.each(data, function (k,v) {
                 _items += "<option value='"+v.city_id+"'>"+v.city_name+"</option>";
              });
              $('#city_id').html(_items);
            }
        });
});


  $('#prov_id1').on('change', function (v) {
    var _items = "";
    $.ajax({
            type: 'GET',
            url: base_url+'/select/kab/'+ this.value,
            success: function (res) {
              var data = $.parseJSON(res);
              _items = "<option value=''>Pilih Kota/Kabupaten</option>";
              $.each(data, function (k,v) {
                 _items += "<option value='"+v.city_id+"'>"+v.city_name+"</option>";
              });
              $('#city_id1').html(_items);
            }
        });
});
  function processInsert() {
    _create_form.parsley().validate();
    var _form_data = new FormData(_create_form[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
   if(_create_form.parsley().isValid()){
       // submitdata('Nazhir',_form_data,t_nazhir,_create_modal,_create_form[0]);


     $.ajax({
        type: 'POST',
        url: 'Nazhir',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                _create_modal.modal('hide');
                //hideLoading(true);
                showInfo(parse.msg);
                _create_form[0].reset();
            }else if(parse.code == 2){
                //hideLoadingPartial(true);
                //showError(parse.msg);
                _create_modal.modal('hide');
                swal("Info!", "Task Untuk "+ parse.msg +" Sudah Ada", "info");
                _create_form[0].reset();
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    }).always(function () {
        var loader = $('.ui.dimmable #mainLoader');
    loader.dimmer({closable: false}).dimmer('show');
       location.reload();
    });

    }

}

function editshow(id) {

$.ajax({
        url: base_url+ '/nazhir/edit/'+id,
        type: 'GET',
        success: function (res) {
            var data = $.parseJSON(res);
            $.each(data, function (k,v) {

                $('#id').val(v.users_id);
                $('#comp_name1').val(v.comp_name);
                $('#comp_desc1').val(v.comp_desc);
                $('#about1').val(v.about);
                $('#prov_id1').val(v.prov_id);
                $('#city_id1').val(v.city_id);

                $('#zzzzz').val(v.city_id);
                $('#admin_name1').val(v.admin_name);
                $('#email1').val(v.email);
                $('#phone_no1').val(v.phone_no);
                $('#alamat1').val(v.alamat);

                $("#logo_lama").attr("src","{{asset('/uploadimg/')}}/"+v.logo);
                $('#data_logo_lama').val(v.logo);

                var data_city = v.city_id;

                $.ajax({
                      type: 'GET',
                      url: base_url+'/select/kab/'+ v.prov_id,
                      success: function (res) {
                        var data = $.parseJSON(res);
                        _items = "<option value=''>Pilih Kota/Kabupaten</option>";
                        $.each(data, function (k,v) {
                           _items += "<option value='"+v.city_id+"'>"+v.city_name+"</option>";
                        });
                        $('#city_id1').html(_items);
                        $('#city_id1').val(data_city);

                      }
                  });



            });
        }
    });
$('#_edit').modal('show');
}


function hapus(id,url) {

    swal({
            title: "Anda Yakin?",
            text: "Data Akan Dihapus!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                  del(id,url);
              } else {
                swal("Cancelled", "Your imaginary file is safe", "error");
            }
        });
}

function del(id,url) {
    url = url + '/' + id;
    $('.modal').each(function () {
        $(this).modal('hide');
    });
    showLoading();
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        if (response.rc == 0) {
            // dataTable.ajax.reload();
           // t_city.draw();
            swal("Berhasil!", "Data Sudah Dihapus", "success");
             var loader = $('.ui.dimmable #mainLoader');
            loader.dimmer({closable: false}).dimmer('show');
            location.reload();
        } else {
            swal("Gagal", response.rm, "error");
        }
    }).fail(function (response) {
        hideLoading(false);
        swal("Error", "Terjadi Kesalahan", "error");
    });
}

function processUpdate() {
    var form = $('#form_edit_lm'),
        formData = new FormData(form[0]);

        for (var pair of formData.entries()) {
            console.log(pair[0]+ ', ' + pair[1]);
        }
   form.parsley().validate();
     //alert('ab');
    if (form.parsley().isValid()) {
      //  alert('a');
        submitdata('nazhir/update',formData,t_nazhir,modal_upd,form[0]);

    }


}

</script>

@stop
