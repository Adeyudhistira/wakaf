@section('pageTitle', 'Laporan')
@section('content')
 <div class="content-body">

 <div class="row">
     <div class="col-12">
        <div class="card">
          <div class="card-header bg-success">
                <h4 class="card-title white">Laporan Transaksi Wakaf Berdasarkan Nazhir</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements white">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
        @php
    $kat = \DB::select("SELECT * FROM reff_category");
    $st = \DB::select("SELECT * FROM reff_status");
    $prov = \DB::select("SELECT * FROM reff_province");
    $nazhir = \DB::select("SELECT * FROM nazhir");
    @endphp    
        <div class="card-content collpase show">
            <div class="card-body card-dashboard">
              <form class="form-horizontal" id="form-filter" method="GET">
               <div class="form-group {{ $errors->has('users_id') ? 'has-error' : ''}} col-md-10 mb-2">
              <label for="users_id">Pilih Nazhir</label>
              <select class="form-control" id="users_id" name="users_id">
                <option value="">Pilih Nazhir</option>
                @foreach($nazhir as $item)
                <option value="{{$item->users_id}}">{{$item->comp_name}}</option>
                @endforeach
              </select>
            </div>
              <div class="form-group {{ $errors->has('tglmulai') ? 'has-error' : ''}}">
                <label class="control-label mb-10 col-sm-2">Tanggal Mulai</label>
                <div class="col-sm-10">
                  <input type="date" required class="form-control"  id="tglmulai" name="tglmulai">
                </div>
              </div>
              <div class="form-group {{ $errors->has('tglselesai') ? 'has-error' : ''}}">
                <label class="control-label mb-10 col-sm-2">Tanggal Selesai</label>
                <div class="col-sm-10">
                  <input type="date" required class="form-control" id="tglselesai" name="tglselesai">
                </div>
              </div>
              <div class="form-group {{ $errors->has('status_project') ? 'has-error' : ''}} col-md-10 mb-2">
              <label for="status_project">Status Project</label>
              <select class="form-control" id="status_project" name="status_project">
                <option value="">Pilih Status</option>
                @foreach($st as $item)
                <option value="{{$item->id}}">{{$item->definition}}</option>
                @endforeach
              </select>
            </div>
              <div class="modal-footer">
                     <button type="submit" value="cari" id="filtercari" name='filtercari' class="btn btn-danger">Tampilkan Data</button>
                  </div>
             </form>

                </div>
            </div>

             <div class="card-content collpase show">
                <div class="card-body card-dashboard">
                  
                  <table class="table table-hover mb-0" width="100%" id="tbl">
                   <thead>
                    <th>Nama Nazhir</th>
                    <th>Nama Project</th>
                    <th>No VA</th>
                    <th>Nominal Wakaf</th>
                    <th>Status</th>
                  </thead>
                  @if($flt)
                  <tbody>
                    @foreach($data as $item)
                    <tr>
                      <td>{{$item->comp_name}}</td>
                      <td>{{$item->nama_project}}</td>
                      <td>{{$item->no_va}}</td>
                      <td style="text-align: right;">{{number_format($item->nominal)}}</td>
                      <td>{{$item->status}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    @endif
                  
                    </table>
                </div>
            </div>


        </div>
    </div>
  </div>

</div>

@endsection
@section('script')
<script type="text/javascript">
    var table = $('#tbl').DataTable({
    width:100,
    scrollX: true,
    searching: false
    
});
</script>
<script type="text/javascript" src="{{ asset('assets/js/_project.js') }}"></script>
@stop