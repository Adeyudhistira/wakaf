<div class="modal animated slideInRight text-left" id="_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
        <h4 class="modal-title white" id="myModalLabel76">Edit Project</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
  <div class="modal-body">
    @php
    $kat = \DB::select("SELECT * FROM reff_category");
    $st = \DB::select("SELECT * FROM reff_status");
    $prov = \DB::select("SELECT * FROM reff_province");
    $nazhir = \DB::select("SELECT * FROM nazhir");
    @endphp
    <form class="form" id="form_edit_lm">
      <div class="form-body">
        <h4 class="form-section"><i class="fa fa-desktop"></i> Data Project</h4>
          <div class="row">
            <div class="form-group col-md-6 mb-2">
              <label for="users_id">Pilih Nazhir</label>
              <select class="form-control" id="users_id1" name="users_id" data-parsley-required>
                <option value="">Pilih Nazhir</option>
                @foreach($nazhir as $item)
                <option value="{{$item->users_id}}">{{$item->comp_name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="nama_project">Nama Project</label>
               <input id="nama_project1" class="form-control border-success " placeholder="Project" name="nama_project" type="text" data-parsley-required>
               <input id="id" class="form-control border-success " name="id" type="hidden">
            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="deskripsi_project">Deskripsi Project</label>
              <textarea class="form-control border-success" id="deskripsi_project1" name="deskripsi_project" data-parsley-required></textarea>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="category_id">Kategory</label>
              <select class="form-control" id="category_id1" name="category_id" data-parsley-required>
                <option value="">Pilih Kategory</option>
                @foreach($kat as $item)
                <option value="{{$item->category_id}}">{{$item->category_desc}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="target_dana">Target Dana</label>
               <input id="target_dana1" onkeyup="convertToRupiah(this)"  class="form-control border-success " placeholder="Project" name="target_dana" type="text" data-parsley-required>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="crt_dt">Tanggal Mulai Project</label>
               <input id="crt_dt1" class="form-control border-success" name="crt_dt" type="date" data-parsley-required>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="end_date">Tanggal Selesai Project</label>
               <input id="end_date1" class="form-control border-success" name="end_date" type="date" data-parsley-required>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="prov_id">Provinsi</label>
              <select class="form-control" id="prov_id1" name="prov_id" data-parsley-required>
                <option value="">Pilih Provinsi</option>
                @foreach($prov as $item)
                <option value="{{$item->prov_id}}">{{$item->prov_name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="city_id">Kota/Kabupaten</label>
              <select class="form-control" id="city_id1" name="city_id" data-parsley-required>
                <option value="">Pilih Kota/Kabupaten</option>
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="status_project">Status Project</label>
              <select class="form-control" id="status_project1" name="status_project" data-parsley-required>
                <option value="">Pilih Status</option>
                @foreach($st as $item)
                <option value="{{$item->id}}">{{$item->definition}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="status_project">Jenis Wakaf</label>
              <select class="form-control" id="jenis_wakaf1" name="jenis_wakaf" data-parsley-required>
                <option value="">Pilih Jenis</option>
                <option value="Wakaf Uang">Wakaf Uang</option>
                <option value="Wakaf Dengan Uang">Wakaf Dengan Uang</option>
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="logo">Image Utama Project</label>
              <img id="logo_lama" width="150" height="150" style="display:block;border:1px solid #e8e8e8;" src="" alt="">
              <input id="data_logo_lama" type="hidden" name="logo_lama" value="">
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="status_project">Upload Image Utama Project</label>
               <input id="logo" class="form-control border-success" placeholder="Logo" name="logo" type="file">
            </div>
          </div>
          <h4 class="form-section"><i class="fa fa-user"></i> Gallery Project</h4>
          <div class="row">
            <div class="col-md-4">
              <div class="row" >
                <div class="form-group col-md-12 mb-2">
                  <label for="logo">Gallery Project 1</label>
                  <img id="gp_lama1" width="150" height="150" style="display:block;border:1px solid #e8e8e8;" src="" alt="">
                  <input id="data_gp_lama1" type="hidden" name="gp_lama1" value="">
                </div>
                <div class="form-group col-md-12 mb-2">
                  <label for="status_project">Upload Gallery Project 1</label>
                   <input id="img1" class="form-control border-success" name="img_satu" type="file" data-parsley-required>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="row">
                <div class="form-group col-md-12 mb-2">
                  <label for="logo">Gallery Project 2</label>
                  <img id="gp_lama2"width="150" height="150" style="display:block;border:1px solid #e8e8e8;" src="" alt="">
                  <input id="data_gp_lama2" type="hidden" name="gp_lama2" value="">
                </div>
                <div class="form-group col-md-12 mb-2">
                  <label for="status_project">Upload Gallery Project 2</label>
                   <input id="img2" class="form-control border-success" name="img_dua" type="file" data-parsley-required>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="row">
                <div class="form-group col-md-12 mb-2">
                  <label for="logo">Gallery Project 3</label>
                  <img id="gp_lama3" width="150" height="150" style="display:block;border:1px solid #e8e8e8;" src="" alt="">
                  <input id="data_gp_lama3" type="hidden" name="gp_lama3" value="">
                </div>
                <div class="form-group col-md-12 mb-2">
                  <label for="status_project">Upload Gallery Project 3</label>
                   <input id="img3" class="form-control border-success"  name="img_tiga" type="file" data-parsley-required>
                </div>
              </div>
            </div>



          </div>
      </div>
    </form>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
    <button type="button" class="btn btn-outline-success"  onclick="processUpdate()">Simpan</button>
  </div>
</div>
</div>
</div>
