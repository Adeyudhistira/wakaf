@section('pageTitle', 'Project')
@section('content')
  <div class="loading-mail">
    <div class="container">
      <div class="myprogress">
        <span>Mohon Tunggu, Dalam Proses Penyimpanan Data ...</span>
          <div id="loaderzz"></div>
      </div>
    </div>


  </div>

 <div class="content-body">
 <div class="row">
     <div class="col-12">

        <a href="#" onclick="tambah()" class="btn btn-social bg-success width-200 mr-1 mb-1 btn-dropbox">
                            <span class="fa fa-plus font-medium-3"></span> Tambah Project</a>
        <div class="card">
          <div class="card-header bg-success">
                <h4 class="card-title white">Data Project</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements white">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
        <div class="card-content collpase show">
            <div class="card-body card-dashboard">

                    <table id="t_project" class="table table-striped mb-0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Img</th>
                          <th>Nama Project</th>
                          <th>Category</th>
                          <th>Province</th>
                          <th>City</th>
                          <th>Nazhir</th>
                          <th>Jenis Wakaf</th>
                          <th>Target Dana</th>
                          <th>Tanggal Mulai</th>
                          <th>Tanggal Selesai</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $item)
                        <tr>
                          <td>{{$item->nomor_urut}}</td>
                          <td>
                            @if($item->main_img)
                            <figure class="card card-img-top border-grey border-lighten-2" itemprop="associatedMedia"
                              itemscope itemtype="http://schema.org/ImageObject">
                                <a href="{{asset('/uploadimg/')}}/{{ $item->main_img }}"  itemprop="contentUrl" data-size="280x160">
                                  <img class="gallery-thumbnail card-img-top" src="{{asset('/uploadimg/')}}/{{ $item->main_img }}"
                                  itemprop="thumbnail" alt="Image description" />
                                </a>
                              </figure>
                              @else
                              <figure class="card card-img-top border-grey border-lighten-2" itemprop="associatedMedia"
                              itemscope itemtype="http://schema.org/ImageObject">
                                <a href="{{asset('images/gallery/1.jpg')}}" itemprop="contentUrl" data-size="280x160">
                                  <img class="gallery-thumbnail card-img-top" src="{{asset('images/gallery/1.jpg')}}"
                                  itemprop="thumbnail" alt="Image description" />
                                </a>
                              </figure>
                              @endif
                          </td>
                          <td>{{$item->nama_project}}</td>
                          <td align="center">{{$item->category_desc}}</td>
                          <td align="center">{{$item->prov_name}}</td>
                          <td align="center">{{$item->city_name}}</td>
                          <td align="center">{{$item->comp_name}}</td>
                          <td align="center">{{$item->jenis_wakaf}}</td>
                          <td align="center">{{number_format($item->target_dana)}}</td>
                          <td align="center">{{date('d M Y',strtotime($item->crt_dt))}}</td>
                          <td align="center">{{date('d M Y',strtotime($item->end_date))}}</td>
                          <td nowrap>
                            <button type="button" class="btn btn-success bg-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detail" onclick="detail({{$item->project_id}});">
                        <i class="fa fa-list"></i>
                    </button>
                            <button type="button" class="btn btn-info bg-blue" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" onclick="editshow({{$item->project_id}})">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" class="btn btn-danger bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus" onclick="hapus({{$item->project_id}},base_url+'/project/delete');">
                        <i class="fa fa-trash"></i>
                    </button></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@include('project.add')
@include('project.edit')
@endsection
@section('script')

<script type="text/javascript" src="{{ asset('assets/js/_project.js') }}"></script>
@stop
