@section('pageTitle', 'Project')
@section('content')
 <div class="content-body">
 <div class="row">
     <div class="col-12">
        <div class="card">
          <div class="card-header">
                <h4 class="card-title">Detail Project</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>

            @php
              // var_dump($gallery);
              // die();
            @endphp
        <div class="card-content collpase show">
            <div class="card-body card-dashboard">
                  <div class="row">
                    <div class="form-group col-md-6 mb-2">
                      <label for="nama_project">Nama Project</label>
                       <input id="nama_project" value="{{$data[0]->nama_project}}" readonly class="form-control border-success" type="text">
                    </div>
                    <div class="form-group col-md-6 mb-2">
                      <label for="nama_project">Kategori</label>
                       <input id="nama_project" value="{{$data[0]->category_desc}}" readonly class="form-control border-success" type="text">
                    </div>
                    <div class="form-group col-md-12 mb-2">
                      <label for="nama_project">Deskripsi Project</label>
                       <textarea readonly class="form-control border-success">
                         {{$data[0]->deskripsi_project}}
                       </textarea>
                    </div>
                    <div class="form-group col-md-6 mb-2">
                      <label for="nama_project">Provinsi</label>
                       <input id="nama_project" value="{{$data[0]->prov_name}}" readonly class="form-control border-success" type="text">
                    </div>
                    <div class="form-group col-md-6 mb-2">
                      <label for="nama_project">Kota / Kabupaten</label>
                       <input id="nama_project" value="{{$data[0]->city_name}}" readonly class="form-control border-success" type="text">
                    </div>
                    <div class="form-group col-md-6 mb-2">
                      <label for="nama_project">Nazhir</label>
                       <input id="nama_project" value="{{$data[0]->comp_name}}" readonly class="form-control border-success" type="text">
                    </div>
                    <div class="form-group col-md-6 mb-2">
                      <label for="nama_project">Target Dana</label>
                       <input id="nama_project" value="{{number_format($data[0]->target_dana)}}" readonly class="form-control border-success" type="text">
                    </div>
                    <div class="form-group col-md-6 mb-2">
                      <label for="nama_project">Tanggal Mulai</label>
                       <input id="nama_project" value="{{date('d M Y',strtotime($data[0]->crt_dt))}}" readonly class="form-control border-success" type="text">
                    </div>
                    <div class="form-group col-md-6 mb-2">
                      <label for="nama_project">Tanggal Selesai</label>
                       <input id="nama_project" value="{{date('d M Y',strtotime($data[0]->end_date))}}" readonly class="form-control border-success" type="text">
                    </div>
                    <div class="form-group col-md-6 mb-2">
                      <label for="nama_project">Status Project</label>
                       <input id="nama_project" value="{{ ($data[0]->status_project==1) ? 'Aktif' : 'Tidak Aktif' }}" readonly class="form-control border-success" type="text">
                    </div>
                    <div class="form-group col-md-6 mb-2">
                      <label for="nama_project">Jenis Wakaf</label>
                       <input id="nama_project" value="{{$data[0]->jenis_wakaf}}" readonly class="form-control border-success" type="text">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 mt-3 mb-5">
                      <label>Gallery Project 1</label>
                      <img class="gallery-thumbnail card-img-top" src="{{asset('/uploadimg/')}}/{{ $data[0]->img1 }}"
                      itemprop="thumbnail" alt="Image description" />
                    </div>
                    <div class="col-md-4 mt-3">
                      <label>Gallery Project 2</label>
                      <img class="gallery-thumbnail card-img-top" src="{{asset('/uploadimg/')}}/{{ $data[0]->img2 }}"
                      itemprop="thumbnail" alt="Image description" />
                    </div>
                    <div class="col-md-4 mt-3">
                      <label>Gallery Project 3</label>
                      <img class="gallery-thumbnail card-img-top" src="{{asset('/uploadimg/')}}/{{ $data[0]->img3 }}"
                      itemprop="thumbnail" alt="Image description" />
                    </div>
                  </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('assets/js/_project.js') }}"></script>
@stop
