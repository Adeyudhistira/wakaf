<div class="modal animated slideInRight text-left" id="_create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel76" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
        <h4 class="modal-title white" id="myModalLabel76">Tambah Project</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
  <div class="modal-body">
    @php
    $kat = \DB::select("SELECT * FROM reff_category");
    $st = \DB::select("SELECT * FROM reff_status");
    $prov = \DB::select("SELECT * FROM reff_province");
    $nazhir = \DB::select("SELECT * FROM nazhir");
    @endphp
    <form class="form" id="_create_form">
      <div class="form-body">
        <h4 class="form-section"><i class="fa fa-desktop"></i> Data Project</h4>
          <div class="row">
            <div class="form-group col-md-6 mb-2">
              <label for="users_id">Pilih Nazhir</label>
              <select class="form-control" id="users_id" name="users_id" data-parsley-required>
                <option value="">Pilih Nazhir</option>
                @foreach($nazhir as $item)
                <option value="{{$item->users_id}}">{{$item->comp_name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="nama_project">Nama Project</label>
               <input id="nama_project" class="form-control border-success " placeholder="Project" name="nama_project" type="text" data-parsley-required>
            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="deskripsi_project">Deskripsi Project</label>
              <textarea class="form-control border-success" id="deskripsi_project" name="deskripsi_project" data-parsley-required></textarea>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="category_id">Kategory</label>
              <select class="form-control" id="category_id" name="category_id" data-parsley-required>
                <option value="">Pilih Kategory</option>
                @foreach($kat as $item)
                <option value="{{$item->category_id}}">{{$item->category_desc}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="target_dana">Target Dana</label>
               <input id="target_dana" onkeyup="convertToRupiah(this)"  class="form-control border-success " placeholder="Project" name="target_dana" type="text" data-parsley-required>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="crt_dt">Tanggal Mulai Project</label>
               <input id="crt_dt" class="form-control border-success" name="crt_dt" type="date" data-parsley-required>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="end_date">Tanggal Selesai Project</label>
               <input id="end_date" class="form-control border-success" name="end_date" type="date" data-parsley-required>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="prov_id">Provinsi</label>
              <select class="form-control" id="prov_id" name="prov_id" data-parsley-required>
                <option value="">Pilih Provinsi</option>
                @foreach($prov as $item)
                <option value="{{$item->prov_id}}">{{$item->prov_name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="city_id">Kota/Kabupaten</label>
              <select class="form-control" id="city_id" name="city_id" data-parsley-required>
                <option value="">Pilih Kota/Kabupaten</option>
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="status_project">Status Project</label>
              <select class="form-control" id="status_project" name="status_project" data-parsley-required>
                <option value="">Pilih Status</option>
                @foreach($st as $item)
                <option value="{{$item->id}}">{{$item->definition}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-6 mb-2">
              <label for="status_project">Jenis Wakaf</label>
              <select class="form-control" id="jenis_wakaf" name="jenis_wakaf" data-parsley-required>
                <option value="">Pilih Jenis</option>
                <option value="Wakaf Uang">Wakaf Uang</option>
                <option value="Wakaf Dengan Uang">Wakaf Dengan Uang</option>
              </select>
            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="status_project">Upload Image Utama Project</label>
               <input id="logo" class="form-control border-success" placeholder="Logo" name="logo" type="file" data-parsley-required>
            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="status_project">Giro Penampung</label>
               <input id="giro" class="form-control border-success" placeholder="Giro Penampung" minlength="2" maxlength="25" name="giro" type="text" data-parsley-required>
            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="status_project">Giro Transaksi</label>
               <input id="giro1" class="form-control border-success" placeholder="Giro Transaksi" minlength="2" maxlength="25" name="giro1" type="text" data-parsley-required>
            </div>
          </div>
          <h4 class="form-section"><i class="fa fa-user"></i> Gallery Project</h4>
          <div class="row">
            <div class="form-group col-md-12 mb-2">
              <label for="img1">Upload Gallery Project 1</label>
               <input id="img1" class="form-control border-success" placeholder="Logo" name="img1" type="file" data-parsley-required>
            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="img2">Upload Gallery Project 2</label>
               <input id="img2" class="form-control border-success" placeholder="Logo" name="img2" type="file" data-parsley-required>
            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="img3">Upload Gallery Project 3</label>
               <input id="img3" class="form-control border-success" placeholder="Logo" name="img3" type="file" data-parsley-required>
            </div>
          </div>
      </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
    <button type="button" class="btn btn-outline-success"  onclick="processInsert()">Simpan</button>
  </div>
</div>
</div>
</div>
<script type="text/javascript">




document.getElementById("crt_dt").onchange = function () {
    var input = document.getElementById("end_date");
    input.setAttribute("min", this.value);
  document.getElementById("end_date").value = this.value;
}

// $(document).ready(function(){
//   $('[id="simpan_load"]').click(function(){
//     $('.loading-mail').delay(0).fadeIn('slow');
//   });
// });


  function convertToRupiah (objek) {
 separator = ",";
 a = objek.value;
 b = a.replace(/[^\d]/g,"");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
 j = j + 1; if (((j % 3) == 1) && (j != 1)) {
 c = b.substr(i-1,1) + separator + c; } else {
 c = b.substr(i-1,1) + c; } } objek.value = c;
}
</script>
