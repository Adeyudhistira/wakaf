<div class="modal animated slideInRight text-left" id="_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-success">
        <h4 class="modal-title white" id="myModalLabel76">Edit City</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
  <div class="modal-body">
    <form class="form" id="form_edit_lm">
        <input id="id" class="form-control border-success" name="id" type="hidden">
      <div class="form-body">
        <h4 class="form-section"><i class="fa fa-desktop"></i> Data City</h4>

          <div class="row">
               @php

              $data = \DB::select("SELECT * FROM reff_province");
            @endphp
            <div class="form-group col-md-12 mb-2">
              <label for="province1">Province</label>
              <select class="form-control" id="prov_id1" name="province">
                <option value="">Pilih Province</option>
                @foreach($data as $item)
                <option value="{{$item->prov_id}}">{{$item->prov_name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="city_type">City Type</label>
              <input id="city_type1" class="form-control border-success " placeholder="city_type" name="city_type" type="text" data-parsley-required>
            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="city_name1">City</label>
              <input id="city_name1" class="form-control border-success " placeholder="city_name" name="city_name" type="text" data-parsley-required>

            </div>
            <div class="form-group col-md-12 mb-2">
              <label for="postal_code1">Postal Code</label>
              <input id="postal_code1" class="form-control border-success " placeholder="postal_code" name="postal_code" type="text" data-parsley-required>
            </div>
          </div>
      </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
    <button type="button" class="btn btn-outline-success"  onclick="processUpdate()">Simpan</button>
  </div>
</div>
</div>
</div>
