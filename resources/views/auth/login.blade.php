<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
  <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
  <meta name="author" content="PIXINVENT">
  <title>WAKAF | Login</title>
  <link href="{{ asset('images/ico/apple-icon-120.png') }}" rel="apple-touch-icon">
  <link href="{{asset('images/bjbsmall.jpg')}}" rel="shortcut icon" type="image/x-icon">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,400,500,700"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link href="{{ asset('css/vendors.css') }}" rel="stylesheet">
  <!-- END VENDOR CSS-->
  <!-- BEGIN ROBUST CSS-->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <link href="{{ asset('css/pages/login-register.css') }}" rel="stylesheet">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
 <!-- END Custom CSS-->
</head>
<body class="vertical-layout vertical-compact-menu 1-column   menu-expanded blank-page blank-page"
data-open="click" data-menu="vertical-compact-menu" data-col="1-column">
  <!-- ///////////////////////////////////////////////////////background-size:contain;/////////////////////-->
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body" style="opacity: 1.2">
        <section class="flexbox-container">
          <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-4 col-10 box-shadow-2 p-0">

              <div class="card m-0" style="background: #ffffff57 !important;">
                <div class="card-header border-0" style="background-color: initial !important;">
                  <div class="card-title text-center">
                    <div class="p-1">
                     <img width="200" src="{{asset('images/bjbs.png')}}" alt="">
                    </div>
                  </div>
                  <h6 class="card-subtitle text-muted text-center font-small-3 pt-2">
                     <img src="{{asset('images/wakaf.png')}}" style="width:300px;" alt="">
                  </h6>
                </div>
                <div class="card-content">
                  <div class="card-body">
                    <form class="form-horizontal form-simple" method="POST" action="{{ route('login') }}" novalidate>
                        {{ csrf_field() }}
                      <fieldset class="form-group position-relative has-icon-left mb-0">
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-lg input-lg " style="background:#ffffffe3 !important;" id="user_name" placeholder="E-Mail Address"
                        required name="email" value="{{ old('email') }}" autofocus>
                        <div class="form-control-position">
                          <i class="ft-user"></i>
                        </div>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </fieldset>
                      <fieldset class="form-group position-relative has-icon-left">
                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg input-lg" style="background:#ffffffe3 !important;" id="psw"
                        placeholder="Enter Password" name="password" required>
                        <div class="form-control-position">
                          <i class="fa fa-key"></i>
                        </div>
                        @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                          @endif
                      </fieldset>
                      <div class="form-group row">
                        <div class="col-md-6 col-12 text-center text-md-left">

                        </div>
                      </div>
                      <button type="submit" class="btn btn-outline-success btn-block" style="border-color: #ffffff !important;background-color: #ffffffe3 !important;color: #717171 !important;"><i class="ft-unlock"></i> Login</button>
                    </form>
                  </div>
                </div>
                <div class="card-footer" style="background-color: initial !important;border:0;">
                  <div class="">
                  </div>
                </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <!-- BEGIN VENDOR JS-->

  <!-- END ROBUST JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{ asset('js/scripts/forms/form-login-register.js') }}"></script>
  <!-- END PAGE LEVEL JS-->
   <style type="text/css">
        body {
          margin: 0;
          padding: 0;
          overflow: hidden;
          background: url('http://wakaf.basys.id/images/Webimg2.jpg');
          background-size:cover;
          background-position-y:center;
        }
    </style>
</body>
</html>
